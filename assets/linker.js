(function (window) {
	let url = new URL(window.location.href);
	let exists = url.searchParams.get("slgf");
	let slgf = exists != null ? exists : sessionStorage.getItem("slgf");

	if (slgf != null) {
		sessionStorage.setItem("slgf", slgf);

		$.ajax({
			url: '/cart/update.js',
			type: 'POST',
			data: {
				attributes: { "__slgf": slgf }
			},
		});

		if (exists == null) {
			url.searchParams.append('slgf', slgf);
			window.history.pushState({ path: url.href }, '', url.href);
		}

		primeira = localStorage.getItem("visita_slgf") == slgf ? 1 : 0;

		if (primeira == 1) {
			localStorage.setItem("visita_slgf", slgf);
		}

		var xhttp = new XMLHttpRequest();
		xhttp.open("POST", "https://slingfy.com/geral/contadorVisitas", true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send("hash=" + slgf + "&primeira=" + primeira);
	}
})(window);