<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>A Sling thing?</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="/assets/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    .image-thumb {
      width: 200px;
    }
  </style>
</head>

<body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php $this->load->view("lojas/side_bar") ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Produtos</h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">

        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Escolha os produtos que estarão disponíveis aos afiliados</h3>
          </div>
          <div class="card-body">
            <?php
            foreach ($produtos as $produto) :
            ?>
              <div class="card cardProduto">
                <div class="card-header">
                  <h3 class="card-title"><?= $produto['title'] ?></h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i class="fas fa-minus"></i></button>
                  </div>
                </div>
                <form>
                  <div class="card-body">


                    <div class="form-group">
                      <label>Preço médio do produto:</label>
                      <input type="text" value="<?= $produto['soma_variants'] ?>" class="form-control precoMedio" disabled>
                      <input type="hidden" value="<?= $produto['soma_variants_cents'] ?>" class="form-control precoMedioCents">
                      <input type="hidden" value="<?= $porcetagem_comissao ?>" class="form-control porcetagemComissao">
                      <input type="hidden" value="<?= $produto['id'] ?>" class="form-control idProduto">
                      <small class="form-text text-muted">Valor da Soma de todas variantes dividido pela quantidade de variantes</small>
                    </div>
                    <div class="form-group">
                      <label>Porcentagem comissão</label>
                      <input type="text" class="form-control valorComissao" placeholder="Ex: 10%">
                    </div>
                    <div class="form-group">
                      <label>Você Paga:</label>
                      <input type="text" class="form-control valorLojista" placeholder="" disabled>
                    </div>
                    <div class="form-group">
                      <label>Afiliado recebe:</label>
                      <input type="text" class="form-control valorAfiliado" placeholder="" disabled>
                    </div>
                    <?php foreach ($produto['images'] as $image) : ?>
                      <img src="<?= $image['src'] ?>" alt="..." class="image-thumb img-thumbnail">
                    <?php endforeach; ?>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <label>Tipo afiliação:</label>
                    <select class="form-control tipoAfiliacao">
                      <option value="-1">Não incluído</option>
                      <option value="0">Desativado</option>
                      <option value="1">Pública</option>
                      <option value="2">Privada</option>
                    </select>
                    <small class="form-text text-muted">
                      Tipos:
                      <ul>
                        <li>Não incluído: Primeiro estado, nunca foi incluído no Slingfy</li>
                        <li>Desativado: Não aparece para nenhum afiliado</li>
                        <li>Pública: Disponível para todos afiliados</li>
                        <li>Privada: Disponível para todos afiliados amigos</li>
                      </ul>
                    </small>
                    <p>Qualquer mudança nos parâmetros ocorrerá após 24Hrs</p>
                    <p>A primeira inclusão será realizada imediatamente com os paramêtros escolhidos</p>
                    <button type="button" class="btn btn-primary mt-3 salvarProduto">Salvar</button>
                  </div>
                </form>
                <!-- /.card-footer-->
              </div>
              <!-- /.card -->
            <?php endforeach; ?>

          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.1
      </div>
      <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="/assets/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/assets/adminlte/dist/js/adminlte.min.js"></script>
  <!-- Mask JS -->
  <script src="/assets/maskJs/jquery.mask.js"></script>

</body>
<script>
  $(document).ready(function() {
    $('.valorComissao').mask('##0%', {
      reverse: true
    });


    $(".valorComissao").on("keyup", function() {
      let parent = $(this).parents(".cardProduto");
      let lojista = parent.find(".valorLojista");
      let afiliado = parent.find('.valorAfiliado');
      let precoMedioCents = parent.find('.precoMedioCents').val();
      let porcetagemComissaoSlingfy = parent.find('.porcetagemComissao').val();
      let valorComissao = $(this).cleanVal();

      let parte_slinfy = precoMedioCents * (porcetagemComissaoSlingfy / 100);
      precoMedioCents = precoMedioCents - parte_slinfy;

      let multiplicador = valorComissao / 100;
      let afiliadoValor = precoMedioCents * multiplicador;
      let lojistaValor = parte_slinfy + afiliadoValor;

      lojista.val(formatReal(parseInt(lojistaValor)));
      afiliado.val(formatReal(parseInt(afiliadoValor)));

    });

    $(".salvarProduto").on("click", function() {
      console.log("a");
    })
  });

  function formatReal(int) {
    var tmp = int + '';
    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
    if (tmp.length > 6)
      tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

    return "R$ " + tmp;
  }
</script>

</html>