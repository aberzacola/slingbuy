<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>A Sling thing?</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="/assets/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php $this->load->view("lojas/side_bar") ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1><?= $this->lang->line("pagamentos") ?></h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <input type="hidden" id="recorrenciaSet" value="<?= isset($recorrencia['status']) ? $recorrencia['status'] : 'false'; ?>" />
        <?php if (isset($recorrencia['status']) && $recorrencia['status'] == "pending") : ?>
          <div class="card card-warning">
            <div class="card-header">
              <h3 class="card-title"><?= $this->lang->line('alerta') ?></h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <p><?= $this->lang->line('cobrancaPendente') ?></p>
                  <p><?= $this->lang->line('valorCobranca') ?> U$ <?= $recorrencia['limite'] ?></p>
                  <a href="<?= $recorrencia['url'] ?>" target="_blank"><button type="button" class="btn btn-primary"><?= $this->lang->line('aprovar') ?></button></a>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        <?php elseif (isset($recorrencia['status']) && $recorrencia['status'] == "active") : ?>
          <div class="card card-success">
            <div class="card-header">
              <h3 class="card-title"><?= $this->lang->line('situacaoAtual') ?></h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <p><?= $this->lang->line('voceUtilizou') ?> <span class="font-weight-bold">U$ <?= $recorrencia['utilizado'] ?>/U$ <?= $recorrencia['limite'] ?></span> <?= $this->lang->line('doSeuLimite') ?></p>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        <?php else : ?>
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title"><?= $this->lang->line('alerta') ?></h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <p><?= $this->lang->line("configureSeuPagamento") ?></p>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        <?php endif; ?>
        <!-- /.card -->

        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"><?= $this->lang->line("configPagamentos") ?></h3>
          </div>
          <div class="card-body">
            <div class="alert alert-warning alert-dismissible fade show alertErro d-none" role="alert">
              <span class="msg"></span>
            </div>
            <div class="alert alert-success alert-dismissible fade show alertSucesso d-none" role="alert">
              <span class="msg"></span>
            </div>
            
            <div class="row">
              <div class="col-12">
                <h4><?= $this->lang->line("afiliadoTermos") ?></h4>
                <p><?= $this->lang->line("termos_billing") ?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <p><?=$this->lang->line('alterarValorLimite')?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <form id="limiteForm">
                  <div class="form-group">
                    <label for="limiteGastos"><?= $this->lang->line("limiteDeGasto") ?></label>
                    <input type="text" class="form-control" id="limiteGastos" aria-describedby="limiteGastosHelp">
                    <small id="limiteGastosHelp" class="form-text text-muted"><?= $this->lang->line("limiteGastosHelp") ?></small>
                    <small id="limiteGastosHelp" class="form-text text-muted"><?= $this->lang->line("valor_minimo_pagamento") ?></small>
                  </div>
                  <button type="submit" class="btn btn-primary cadastrarLimite d-none"><?= $this->lang->line("cadastrarLimite") ?></button>
                  <button type="submit" class="btn btn-primary updateLimite d-none"><?= $this->lang->line("atualizarLimite") ?></button>
                </form>
              </div>
            </div>
          </div>
          <!-- /.card-footer-->
        </div>
        <!-- /.card -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.1
      </div>
      <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="/assets/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/assets/adminlte/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="/assets/adminlte/dist/js/demo.js"></script>
  <!-- Block Ui -->
  <script src="/assets/blockui/jquery.blockUI.js"></script>
  <!-- Mask -->
  <script src="/assets/maskJs/jquery.mask.js"></script>
  <!-- Mask Money -->
  <script src="/assets/jquery-maskmoney/src/jquery.maskMoney.js"></script>
  <script>
    $(document).ready(function() {
      let shopIdioma = $("#shopIdioma");
      let limiteGastos = $('#limiteGastos');
      let alertErro = $('.alertErro');
      let alertSucesso = $('.alertSucesso');
      let id_convite = $('#convites');
      let recorrenciaSet = $('#recorrenciaSet');
      let cadastrarLimite = $('.cadastrarLimite');
      let updateLimite = $('.updateLimite');

      $(document).ajaxStop($.unblockUI);
      console.log(recorrenciaSet.val());
      switch (recorrenciaSet.val()) {
        case "active":
          updateLimite.removeClass("d-none");
          break;
        case "false":
          cadastrarLimite.removeClass("d-none");
          break;
        case "pending":
          break;
        default:
          cadastrarLimite.removeClass("d-none");
      }

      limiteGastos.maskMoney({prefix:'U$ ', allowNegative: false, thousands:',', decimal:'.', affixesStay: false});

      $("#limiteForm").on("submit", function(e) {
        e.preventDefault();

        let limite = limiteGastos.maskMoney('unmasked')[0] * 100;
        
        if (limite < 1000) {
          showAlert(alertErro, "<?= $this->lang->line("valor_minimo_pagamento") ?>");
          $.unblockUI()
          return;
        }

        $.blockUI({
          message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
        });

        let url = "/lojistas/pagamento/criarrecorrencia?<?= $_SERVER['QUERY_STRING'] ?>"
        if(recorrenciaSet.val() == "active"){
          url =  "/lojistas/pagamento/atualizaLimite?<?= $_SERVER['QUERY_STRING'] ?>"
        }
        
        $.ajax({
          type: "POST",
          url: url,
          data: {
            limite: limite
          },
          dataType: "json",
          success: function(resposta) {
            if (resposta.success == false) {
              showAlert(alertErro, resposta.msg);
            } else {
              window.open(resposta.data.confirmation_url);
            }
          }
        });

      });

      $(".idioma").on("click", function() {
        $.blockUI({
          message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
        });

        let idioma = $(this).attr('idioma');

        $.ajax({
          type: "POST",
          url: "/geral/mudarIdioma",
          data: {
            idioma: idioma,
            shop: shopIdioma.val()
          },
          dataType: "json",
          success: function(resposta) {
            location.reload();
          }
        });

      });
    });

    function showAlert(seletor, msg, timer = 3000) {
      if (seletor.hasClass("d-none")) {
        seletor.find(".msg").html(msg);
        seletor.removeClass("d-none");
        setTimeout(function() {
          seletor.addClass("d-none");
        }, timer);
      }
    }

    function validoInvalido(seletor, flag) { // flag = true => valido, false => invalido
      if (flag == false) {
        seletor.addClass("is-invalid");
        seletor.removeClass("is-valid");
      } else {
        seletor.removeClass("is-invalid");
        seletor.addClass("is-valid");
      }
    }
  </script>
</body>

</html>