<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Slingfy | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="/assets/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Select2 -->
  <link rel="stylesheet" href="/assets/adminlte/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="/assets/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
</head>

<body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php $this->load->view("lojas/side_bar") ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Dashboard</h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <?php if ($recorrencia['status'] == false || $recorrencia['status'] == 'cancelled' ) : ?> 
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title"><?= $this->lang->line('alerta') ?></h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <p><?= $this->lang->line("configureSeuPagamento") ?></p>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        <?php endif; ?>
        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"><?= $this->lang->line("exportarDados") ?></h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="alert alert-warning alert-dismissible fade show alertErro d-none" role="alert">
              <span class="msg"></span>
            </div>
            <div class="alert alert-success alert-dismissible fade show alertSucesso d-none" role="alert">
              <span class="msg"></span>
            </div>
            <div class="row">
              <div class="col-12">
                <form id="downloadData" method="POST" target="_blank">
                  <input type="hidden" value="<?= $safe_hash ?>" name="safe_hash" />
                  <div class="form-group">
                    <label for="convites"><?= $this->lang->line('convites') ?></label>
                    <select class="form-control select2bs4" name="id_convite" id="convites">
                      <option value="0"><?= $this->lang->line("todosConvites") ?></option>
                      <?php foreach ($convites as $convite) : ?>
                        <option value="<?= $convite['id'] ?>"><?= $convite['nome_afiliado'] ?> - Id: <?= $convite['id'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="modalidade"><?= $this->lang->line('visitasOuOrdens') ?></label>
                    <select class="form-control" id="modalidade">
                      <option value="-1"><?= $this->lang->line('selecione') ?></option>
                      <option value="ordens"><?= $this->lang->line('ordens') ?></option>
                      <option value="visitas"><?= $this->lang->line('visitas') ?></option>
                    </select>
                  </div>
                  <button type="button" id="download" class="btn btn-primary"><?= $this->lang->line("downloadEstatisticas") ?></button>
                </form>
              </div>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
          </div>
          <!-- /.card-footer-->
        </div>
        <!-- /.card -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.1
      </div>
      <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="/assets/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/assets/adminlte/dist/js/adminlte.min.js"></script>
  <!-- Moment js -->
  <script src="/assets/adminlte/plugins/moment/moment.min.js"></script>
  <!-- Block Ui -->
  <script src="/assets/blockui/jquery.blockUI.js"></script>
  <!-- Select2 -->
  <script src="/assets/adminlte/plugins/select2/js/select2.full.min.js"></script>
  <!-- date-range-picker -->
  <script src="/assets/adminlte/plugins/daterangepicker/daterangepicker.js"></script>

  <script>
    $(document).ajaxStop($.unblockUI);
    $(document).ready(function() {
      let error = $(".alertErro");
      let sucesso = $(".alertSucesso");
      let form = $("#downloadData");
      let submit = false;
      let shopIdioma = $("#shopIdioma");

      $('.select2bs4').select2({
        theme: 'bootstrap4'
      });

      form.on("submit", function(e) {
        if (submit == false) {
          e.preventDefault();
        }
        submit = false;
      })
      $("#download").on("click", function(e) {
        let modalidade = $("#modalidade");
        let convites = $("#convites");
        submit = true;

        if (modalidade.val() == -1) {
          showAlert(error, '<?= $this->lang->line("selecioneVisitasOuOrdens") ?>');
          validoInvalido(modalidade, false);
          return false;
        } else {
          validoInvalido(modalidade, true);
        }

        let metodo = "";
        if (modalidade.val() == "visitas") {
          metodo = "visitasXLSX";
        } else {
          metodo = "ordersXLSX";
        }

        form.attr("action", "/lojistas/dashboard/" + metodo);
        form.submit();

      })


      $(".idioma").on("click", function() {
        $.blockUI({
          message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
        });

        let idioma = $(this).attr('idioma');

        $.ajax({
          type: "POST",
          url: "/geral/mudarIdioma",
          data: {
            idioma: idioma,
            shop: shopIdioma.val()
          },
          dataType: "json",
          success: function(resposta) {
            location.reload();
          }
        });

      });
    });


    function numberToReal(numero) {
      var numero = numero.toFixed(2).split('.');
      numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
      return numero.join(',');
    }

    function showAlert(seletor, msg, timer = 3000) {
      if (seletor.hasClass("d-none")) {
        seletor.find(".msg").html(msg);
        seletor.removeClass("d-none");
        setTimeout(function() {
          seletor.addClass("d-none");
        }, timer);
      }
    }

    function validoInvalido(seletor, flag) { // flag = true => valido, false => invalido
      if (flag == false) {
        seletor.addClass("is-invalid");
        seletor.removeClass("is-valid");
      } else {
        seletor.removeClass("is-invalid");
        seletor.addClass("is-valid");
      }
    }
  </script>
</body>

</html>