<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>A Sling thing?</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="/assets/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Simditor -->
  <link rel="stylesheet" type="text/css" href="/assets/simditor/site/assets/styles/simditor.css" />
  <!-- Tabulator -->
  <link href="/assets/tabulator/dist/css/tabulator.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php $this->load->view("lojas/side_bar") ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1><?= $this->lang->line('gestaoDeAfiliadosTitulo') ?></h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="alert alert-warning alert-dismissible fade show alertErro d-none" role="alert">
          <span class="msg"></span>
        </div>
        <div class="alert alert-success alert-dismissible fade show alertSucesso d-none" role="alert">
          <span class="msg"></span>
        </div>
        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"><?= $this->lang->line('convidarAfiliadoTitulo') ?></h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool colapse" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <form id="formConviteAfiliado">
              <input type="hidden" name="shop" id="shop" value="<?= $this->input->get("shop") ?>" />
              <div class="form-group">
                <label for="nomeAfilado"><?= $this->lang->line('afiliadoNomeTitulo') ?></label>
                <input type="text" class="form-control" id="nomeAfilado" name="nomeAfilado" placeholder="<?= $this->lang->line('afiliadoNomePlaceHolder') ?>" required>
              </div>
              <div class="form-group">
                <label for="emailAfiliado"><?= $this->lang->line('afiliadoEmailTitulo') ?></label>
                <input type="email" class="form-control" id="emailAfiliado" name="emailAfiliado" aria-describedby="emailAfiliadoHelp" placeholder="<?= $this->lang->line('afiliadoEmailPlaceholder') ?>" required>
              </div>
              <div class="form-group">
                <label for="termosAfiliado"><?= $this->lang->line('afiliadoTermos') ?></label>
                <small id="termosAfiliadoHelp" class="form-text text-muted"><?= $this->lang->line('afiliadoTermosExplicacao') ?></small>
                <textarea class="form-control" id="termosAfiliado" name="termosAfiliado" rows="2"> </textarea>
              </div>
              <button type="submit" class="btn btn-primary"><?= $this->lang->line('convidarAfiliadoButton') ?></button>
            </form>
          </div>
          <!-- /.card-footer-->
        </div>
        <!-- /.card -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title"><?= $this->lang->line('convitesEnviados') ?></h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div id="convitesEnviados"></div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.1
      </div>
      <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <div class="modal fade" id="modal-termos">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><?= $this->lang->line("termos") ?></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body modalTermos">
          <p>One fine body&hellip;</p>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line("fechar") ?></button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- jQuery -->
  <script src="/assets/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- jQuer UI -->
  <script src="/assets/adminlte/plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/assets/adminlte/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="/assets/adminlte/dist/js/demo.js"></script>
  <!-- Simditor  -->
  <script type="text/javascript" src="/assets/simditor/site/assets/scripts/module.js"></script>
  <script type="text/javascript" src="/assets/simditor/site/assets/scripts/hotkeys.js"></script>
  <script type="text/javascript" src="/assets/simditor/site/assets/scripts/uploader.js"></script>
  <script type="text/javascript" src="/assets/simditor/site/assets/scripts/simditor.js"></script>
  <!-- Tabulator -->
  <script type="text/javascript" src="/assets/tabulator/dist/js/tabulator.js"></script>
  <!-- BlockUI -->
  <script type="text/javascript" src="/assets/blockui/jquery.blockUI.js"></script>

  <script>
    $(document).ajaxStop($.unblockUI);

    $(document).ready(function() {

      let alertErro = $(".alertErro");
      let alertSucesso = $(".alertSucesso");
      let shop = $("#shop");
      let emailAfiliado = $("#emailAfiliado");
      let editor = new Simditor({
        textarea: $('#termosAfiliado')
      });
      let modalTermos = $("#modal-termos");

      var table = new Tabulator("#convitesEnviados", {
        ajaxURL: "/lojistas/afiliados/getTabelaAfiliados",
        ajaxParams: {
          shop: shop.val()
        },
        ajaxConfig: "POST",
        ajaxResponse: function(url, params, response) {
          if (response.success == true) {
            return response.data;
          } else {
            return [];
          }

        },
        layout: "fitColumns",
        columns: [{
            title: "Id",
            field: "id",
            align: "center",
            headerFilter: true
          }, //column has a fixed width of 100px;
          {
            title: "<?= $this->lang->line("nome") ?>",
            field: "nome_afiliado",
            align: "center",
            headerFilter: true
          }, //column will be allocated 1/5 of the remaining space
          {
            title: "Email",
            field: "email_afiliado",
            align: "center",
            headerFilter: true
          }, //column will be allocated 3/5 of the remaining space
          {
            title: "<?= $this->lang->line("termos") ?>",
            field: "termos",
            formatter: "html",
            align: "center",
            cellClick: function(e, cell) {
              //e - the click event object
              let row = cell.getRow();
              let rowData = row.getData();
              modalTermos.modal();
              modalTermos.find(".modalTermos").html(rowData.texto_termo);
            }
          },
          {
            title: "Status",
            field: "status",
            formatter: "html",
            align: "center",
            headerFilter: "select",
            headerFilterParams: {
              values: {
                "<?= $this->lang->line("pendente") ?>": "<?= $this->lang->line("pendente") ?>",
                "<?= $this->lang->line("removido") ?>": "<?= $this->lang->line("removido") ?>",
                "<?= $this->lang->line("aceito") ?>": "<?= $this->lang->line("aceito") ?>",
                "<?= $this->lang->line("recusado") ?>": "<?= $this->lang->line("recusado") ?>",
                "<?= $this->lang->line("cancelado") ?>": "<?= $this->lang->line("cancelado") ?>"
              }
            }
          },
          {
            title: "Texto Termo",
            field: "texto_termo",
            visible: false,
            align: "center"
          },
          {
            title: "Aceito",
            field: "aceito",
            visible: false,
            align: "center"
          },
          {
            title: "<?= $this->lang->line("data") ?>",
            field: "data",
            align: "center",
            headerFilter: true,
            sorterParams: {
              format: "<?= $idioma == "ptb" ? "DD/MM/YYYY HH:mm:ss" : "MM/DD/YYYY HH:mm:ss"; ?>",
              alignEmptyValues: "top",
            }
          },
          {
            title: "<?= $this->lang->line("remover") ?>",
            field: "remover",
            formatter: "html",
            align: "center",
            cellClick: function(e, cell) {
              var confirmation = confirm("<?=$this->lang->line("certezaQueDesejaCancelar")?>");

              if(confirmation == false){
                return;
              }
              let row = cell.getRow();
              let rowData = row.getData();
              if (rowData.aceito != 0 && rowData.aceito != 1) {
                  showAlert(alertErro, "<?= $this->lang->line("convitePendenteErro") ?>");
              } else {
                $.blockUI({
                  message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
                });
                $.ajax({
                  type: "POST",
                  url: "/lojistas/afiliados/cancelarConvite",
                  data: {
                    id_afiliado: rowData.id,
                    shop: shop.val()
                  },
                  dataType: "json",
                  success: function(resposta) {
                    if (resposta.success == false) {
                      showAlert(alertErro, resposta.msg, 6000);
                    } else {
                      showAlert(alertSucesso, resposta.msg, 6000);
                      table.setData();
                    }
                  }
                });
              }
            }
          }
        ]
      });

      emailAfiliado.on("keyup", function() {
        validoInvalido($(this), validateEmail(emailAfiliado.val()))
      })

      $("#formConviteAfiliado").on("submit", function(e) {
        e.preventDefault();
        $.blockUI({
          message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
        });
        let form = $(this);

        $.ajax({
          type: "POST",
          url: "/lojistas/afiliados/convidaAfiliado",
          data: form.serialize(),
          dataType: "json",
          success: function(resposta) {
            if (resposta.success == false) {
              showAlert(alertErro, resposta.msg);
            } else {
              showAlert(alertSucesso, resposta.msg);
              form[0].reset();
              editor.setValue("");
              table.setData();
            }
          }
        });

      });

      $(".idioma").on("click", function() {
        $.blockUI({
          message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
        });

        let idioma = $(this).attr('idioma');

        $.ajax({
          type: "POST",
          url: "/geral/mudarIdioma",
          data: {
            idioma: idioma,
            shop: shop.val()
          },
          dataType: "json",
          success: function(resposta) {
            location.reload();
          }
        });

      });

    });

    function showAlert(seletor, msg, timer = 3000) {
      if (seletor.hasClass("d-none")) {
        seletor.find(".msg").html(msg);
        seletor.removeClass("d-none");
        $("html, body").animate({
          scrollTop: 0
        }, "slow");
        setTimeout(function() {
          seletor.addClass("d-none");
        }, timer);
      }
    }

    function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }

    function validoInvalido(seletor, flag) { // flag = true => valido, false => invalido
      if (flag == false) {
        seletor.addClass("is-invalid");
        seletor.removeClass("is-valid");
      } else {
        seletor.removeClass("is-invalid");
        seletor.addClass("is-valid");
      }
    }
  </script>
</body>

</html>