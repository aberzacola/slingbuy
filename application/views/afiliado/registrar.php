<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Slingfy | <?= $this->lang->line("cadastro") ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="/assets/adminlte/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="/assets/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/assets/adminlte/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition register-page">
    <div class="register-box">
        <div class="register-logo">
            <a href="../../index2.html"><b>Sling</b>fy</a>
        </div>

        <div class="card">
            <div class="card-body register-card-body">
                <p><?= $this->lang->line("escolhaSeuIdioma") ?>:&nbsp
                    <img class="idioma" idioma="ptb" style="cursor:pointer" src="/assets/imgs/brazil.png" />
                    <img class="idioma" idioma="en" style="cursor:pointer" src="/assets/imgs/us.png" />
                </p>
                <p class="login-box-msg"><?= $this->lang->line("cadastrese") ?></p>
                <div class="alert alert-warning alert-dismissible fade show alertErro d-none" role="alert">
                    <span class="msg"></span>
                </div>
                <div class="alert alert-success alert-dismissible fade show alertSucesso d-none" role="alert">
                    <span class="msg"></span>
                </div>
                <form id="formRegistrar" method="post">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="inputNome" name="nome" placeholder="<?= $this->lang->line("nomeCompleto") ?>" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" id="inputEmail" name="email" placeholder="<?= $this->lang->line("email") ?>" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group input-group mb-3">
                        <input type="password" class="form-control" id="inputSenha" data-toggle="tooltip" title="<?= $this->lang->line("dicasSenha") ?>" name="senha" placeholder="<?= $this->lang->line("senha") ?>" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" id="inputResenha" name="resenha" placeholder="<?= $this->lang->line("confirmeASenha") ?>" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <div class="icheck-primary">
                                <input type="checkbox" name="terms" id="checkboxTermos" value="agree">
                                <label for="checkboxTermos">
                                    <?= $this->lang->line("euConcordo") ?> <a href="https://help.slingfy.com/privacy-policy"> <?= $this->lang->line("termos") ?></a>
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <button type="submit" id="buttonCadastrar" class="btn btn-primary btn-block"><?= $this->lang->line("cadastrar") ?></button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <a href="/afiliados/cadastro/login" class="text-center"><?= $this->lang->line("euJaTenhoUmaConta") ?></a>
            </div>
            <!-- /.form-box -->
        </div><!-- /.card -->
    </div>
    <!-- /.register-box -->

    <!-- jQuery -->
    <script src="/assets/adminlte/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/assets/adminlte/dist/js/adminlte.min.js"></script>
    <!-- Block Ui -->
    <script src="/assets/blockui/jquery.blockUI.js"></script>
</body>
<script>
    (function(window, $) {
        $(document).ajaxStop($.unblockUI);

        let inputNome = $("#inputNome");
        let inputEmail = $("#inputEmail");
        let formRegistrar = $("#formRegistrar");
        let inputSenha = $("#inputSenha");
        let inputResenha = $("#inputResenha");
        let checkboxTermos = $("#checkboxTermos");
        let alertErro = $(".alertErro");
        let alertSucesso = $(".alertSucesso");

        $('[data-toggle="tooltip"]').tooltip()

        $(".idioma").on("click", function() {
            $.blockUI({
                message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
            });
            let idioma = $(this).attr('idioma');

            $.ajax({
                type: "POST",
                url: "/geral/mudarIdioma",
                data: {
                    idioma: idioma
                },
                dataType: "json",
                success: function(resposta) {
                    location.reload();
                }
            });

        })


        inputNome.on("keyup", function() {
            validoInvalido($(this), validaNome(inputNome.val()))
        })
        inputEmail.on("keyup", function() {
            validoInvalido($(this), validateEmail(inputEmail.val()))
        })

        inputSenha.on("keyup", function() {
            validoInvalido($(this), $(this).val().length >= 6)
        });
        
        inputResenha.on("keyup", function() {
            validoInvalido($(this), $(this).val().length >= 6)
        });

        formRegistrar.on("submit", function(e) {
            e.preventDefault();

            if (!validaNome(inputNome.val())) {
                showAlert(alertErro, "<?= $this->lang->line("nomeInvalido") ?>");
                return;
            }

            if (!validateEmail(inputEmail.val())) {
                showAlert(alertErro, "<?= $this->lang->line("emailInvalido") ?>");
                return;
            }

            if (inputSenha.val() != inputResenha.val()) {
                showAlert(alertErro, "<?= $this->lang->line("senhasNaoSaoIguais") ?>");
                return;
            }

            if (inputSenha.val().length < 6) {
                showAlert(alertErro, "<?= $this->lang->line("dicasSenha") ?>");
                return;
            }

            if (!checkboxTermos.is(":checked")) {
                showAlert(alertErro, "<?= $this->lang->line("concordeComOsTermos") ?>");
                return;
            }

            let payload = {
                nome: inputNome.val(),
                email: inputEmail.val(),
                senha: inputSenha.val(),
                resenha: inputResenha.val(),
            }

            $.blockUI({
                message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
            });
            $.ajax({
                type: "POST",
                url: "/afiliados/cadastro/cadastra",
                data: payload,
                dataType: "json",
                success: function(resposta) {
                    if (resposta.success == false) {
                        showAlert(alertErro, resposta.msg, 6000);
                    } else {
                        showAlert(alertSucesso, resposta.msg);
                    }
                }
            });
        })



    })(window, jQuery)

    function validaNome(nome) {
        let regexp = new RegExp(/^[a-z]([-']?[a-z]+)*( [a-z]([-']?[a-z]+)*)+$/i);
        return regexp.test(nome.toLowerCase());
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function showAlert(seletor, msg, timer = 3000) {
        if (seletor.hasClass("d-none")) {
            seletor.find(".msg").html(msg);
            seletor.removeClass("d-none");
            setTimeout(function() {
                seletor.addClass("d-none");
            }, timer);
        }
    }

    function validoInvalido(seletor, flag) { // flag = true => valido, false => invalido
        if (flag == false) {
            seletor.addClass("is-invalid");
            seletor.removeClass("is-valid");
        } else {
            seletor.removeClass("is-invalid");
            seletor.addClass("is-valid");
        }
    }   
</script>

</html>