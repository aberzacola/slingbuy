<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Slingfy | <?= $this->lang->line("lojas") ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="/assets/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php $this->load->view("afiliado/side_bar") ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1><?= $this->lang->line("lojas") ?></h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <?php if ($convites_pendentes == true) : ?>
          <div class="card card-warning">
            <div class="card-header">
              <h3 class="card-title"><?= $this->lang->line('alerta') ?></h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <p><?= $this->lang->line("convitesPendentes") ?></p>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        <?php endif; ?>
        <?php
        if ($convites_aceitos !== false)
          foreach ($convites_aceitos as $convites) :
        ?>
          <!-- Default box -->
          <div class="card collapsed-card">
            <div class="card-header">
              <h3 class="card-title"><?= $convites['nome_loja'] ?> - ID: <?= $convites['id'] ?></h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-plus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="alert alert-warning alert-dismissible fade show alertErro d-none" role="alert">
                <span class="msg"></span>
              </div>
              <div class="alert alert-success alert-dismissible fade show alertSucesso d-none" role="alert">
                <span class="msg"></span>
              </div>
              <div class="row">
                <div class="col-12">
                  <h3><?= $this->lang->line('produtos') ?></h3>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <div class="form-group">
                    <label><?= $this->lang->line("filtro") ?></label>
                    <input type="text" class="form-control filtroProdutos" placeholder="<?= $this->lang->line("filtroPlaceholder") ?>">
                  </div>
                </div>
              </div>
              <div class="row mb-4">
                <div class="col-12">
                  <label><?= $this->lang->line("geradorDeLink") ?></label>
                  <div class="input-group input-group">
                    <input type="hidden" class="hash_convite" value="<?= $convites['hash'] ?>" />
                    <input type="text" class="form-control link" placeholder="<?= $this->lang->line("geradorDeLinkPlaceholder") ?>">
                    <span class="input-group-append">
                      <button type="button" class="btn btn-info btn-flat  gerar_link"><?= $this->lang->line("gerar") ?></button>
                    </span>
                  </div>
                </div>
              </div>
              <?php foreach ($convites['produtos'] as $produto) : ?>
                <div class="row filtroFinder">
                  <div class="col-12">
                    <div class="card card-info collapsed-card card-produtos">
                      <div class="card-header">
                        <div class="card-title">
                          <?= $produto['nome'] ?>
                        </div>
                        <div class="card-tools">
                          <button type="button" class="btn btn-tool abreFecha" title="Collapse">
                            <i class="fas fa-plus"></i>
                          </button>
                        </div>
                      </div>
                      <div class="card-body">
                        <div class="row">
                          <div class="col-6">
                            <div class="form-group">
                              <label><?= $this->lang->line("linkDivulgacao") ?></label>
                              <input type="text" onClick="this.setSelectionRange(0, this.value.length)" class="form-control" value='<?= $produto['url'] ?>'>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-12">
                            <label><?= $this->lang->line("imagens") ?></label>
                          </div>
                        </div>
                        <div class="row">
                          <?php
                          foreach ($produto['images'] as $image) :
                          ?>
                            <div class="col-sm-2">
                              <a href="<?= $image['src'] ?>" data-toggle="lightbox" data-title="<?= $image['alt'] ?>" data-gallery="gallery">
                                <img src="<?= $image['src'] ?>" class="img-fluid mb-2" />
                              </a>
                            </div>
                          <?php
                          endforeach;
                          ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php
              endforeach;
              ?>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        <?php
          endforeach;
        ?>

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.1
      </div>
      <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="/assets/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/assets/adminlte/dist/js/adminlte.min.js"></script>
  <!-- Block Ui -->
  <script src="/assets/blockui/jquery.blockUI.js"></script>
  <!-- Ekko Lightbox -->
  <script src="/assets/adminlte/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
  <script>
    $(document).ajaxStop($.unblockUI);
    $(document).ready(function() {

      $(".gerar_link").on("click", function() {
        let parent = $(this).parent();
        let cardBody = $(this).parents(".card-body");
        let conviteHash = parent.siblings(".hash_convite").val();
        let link = parent.siblings(".link");
        let alertSuccess = cardBody.find(".alertSucesso");
        let alertErro = cardBody.find(".alertErro");

        try {
          let url = new URL(link.val());
          let exists = url.searchParams.get("slgf");
          if (exists != null) {
            showAlert(alertErro, "<?= $this->lang->line("jaELinkAfiliado") ?>");
          } else {
            url.searchParams.append('slgf', conviteHash);
            link.val(url.href);
            showAlert(alertSuccess, "<?= $this->lang->line("linkGeradoSucesso") ?>");
          }
        } catch (_) {
          showAlert(alertErro, "<?= $this->lang->line("linkGeradoErro") ?>");
        }
      });

      $(".filtroProdutos").on("keyup", function() {
        let parentRow = $(this).parents(".row")[0];
        let textoBusca = $(this).val();
        let cardProdutos = $(parentRow).siblings(".filtroFinder");
        for (let i = 0; i < cardProdutos.length; i++) {
          let tituloProduto = $(cardProdutos[i]).find(".card-title").html().trim();
          if (tituloProduto.search(textoBusca) == -1) {
            $(cardProdutos[i]).addClass('d-none');
          } else {
            $(cardProdutos[i]).removeClass('d-none');
          }
        }
      })

      $(".card-produtos").on("click", ".abreFecha", function() {
        let cardPai = $(this).parents('.card-produtos')[0];
        if ($(cardPai).hasClass("collapsed-card")) {
          $(this).find("i").removeClass('fa-plus');
          $(this).find("i").addClass('fa-minus');
        } else {
          $(this).find("i").addClass('fa-plus');
          $(this).find("i").removeClass('fa-minus');
        }

        $(cardPai).CardWidget('toggle');

      });

      $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
          alwaysShowClose: true,
          showArrows: false
        });
      });

      $(".idioma").on("click", function() {
        $.blockUI({
          message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
        });

        let idioma = $(this).attr('idioma');

        $.ajax({
          type: "POST",
          url: "/geral/mudarIdioma",
          data: {
            idioma: idioma
          },
          dataType: "json",
          success: function(resposta) {
            location.reload();
          }
        });

      });
    })

    function showAlert(seletor, msg, timer = 3000) {
      if (seletor.hasClass("d-none")) {
        seletor.find(".msg").html(msg);
        seletor.removeClass("d-none");
        setTimeout(function() {
          seletor.addClass("d-none");
        }, timer);
      }
    }
  </script>
</body>

</html>