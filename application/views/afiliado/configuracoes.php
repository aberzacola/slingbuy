<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Slingfy | <?= $this->lang->line("configuracoesAfiliadoTitulo") ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="/assets/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php $this->load->view("afiliado/side_bar") ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1><?= $this->lang->line("configuracoesAfiliadoTitulo") ?></h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">

        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"><?= $this->lang->line("configuracoesTrocarSenha") ?></h3>
          </div>
          <div class="card-body">
            <div class="alert alert-warning alert-dismissible fade show alertErro d-none" role="alert">
              <span class="msg"></span>
            </div>
            <div class="alert alert-success alert-dismissible fade show alertSucesso d-none" role="alert">
              <span class="msg"></span>
            </div>
            <form id="redefinicaoDeSenha">
              <div class="form-group">
                <label for="senhaAtual"><?= $this->lang->line("configuracoesSenhaAtual") ?></label>
                <input type="password" class="form-control" id="senhaAtual" placeholder="<?= $this->lang->line("configuracoesSenhaAtual") ?>" required>
              </div>
              <div class="form-group">
                <label for="novaSenha"><?= $this->lang->line("configuracoesNovaSenha") ?></label>
                <input type="password" class="form-control" id="novaSenha" placeholder="<?= $this->lang->line("configuracoesNovaSenha") ?>" required>
              </div>
              <div class="form-group">
                <label for="confirmaNovaSenha"><?= $this->lang->line("configuracoesNovaSenhaConfirmar") ?></label>
                <input type="password" class="form-control" id="confirmaNovaSenha" placeholder="<?= $this->lang->line("configuracoesNovaSenhaConfirmar") ?>" required>
              </div>
              <button type="submit" id="alterarSenha" class="btn btn-primary"><?= $this->lang->line("alterarSenha") ?></button>
            </form>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.1
      </div>
      <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="/assets/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/assets/adminlte/dist/js/adminlte.min.js"></script>
  <!-- Block Ui -->
  <script src="/assets/blockui/jquery.blockUI.js"></script>
  <script>
    $(document).ajaxStop($.unblockUI);
    $(document).ready(function() {
      let senhaAtual = $("#senhaAtual");
      let novaSenha = $("#novaSenha");
      let confirmaNovaSenha = $("#confirmaNovaSenha");
      let alertErro = $(".alertErro");
      let alertSucesso = $(".alertSucesso");
      let redefinicaoDeSenha = $("#redefinicaoDeSenha");


      confirmaNovaSenha.on("keyup", function() {
        validoInvalido($(this), $(this).val().length >= 6)
      });

      novaSenha.on("keyup", function() {
        validoInvalido($(this), $(this).val().length >= 6)
      });

      redefinicaoDeSenha.on("submit", function(e) {
        e.preventDefault();

        if (novaSenha.val() != confirmaNovaSenha.val()) {
          showAlert(alertErro, '<?= $this->lang->line("senhasNaoSaoIguais") ?>');
          return false;
        }

        if (novaSenha.val().length < 6) {
          showAlert(alertErro, '<?= $this->lang->line("dicasSenha") ?>');
          return false;
        }

        let payload = {
          senhaAtual: senhaAtual.val(),
          novaSenha: novaSenha.val()
        }

        $.blockUI({
          message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
        });

        $.ajax({
          type: "POST",
          url: "/afiliados/cadastro/alterarSenha",
          data: payload,
          dataType: "json",
          success: function(resposta) {
            redefinicaoDeSenha[0].reset();
            if (resposta.success == false) {
              showAlert(alertErro, resposta.msg);
            } else {
              showAlert(alertSucesso, resposta.msg);
            }
          }
        });


      });



    });


    $(".idioma").on("click", function() {
      $.blockUI({
        message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
      });

      let idioma = $(this).attr('idioma');

      $.ajax({
        type: "POST",
        url: "/geral/mudarIdioma",
        data: {
          idioma: idioma
        },
        dataType: "json",
        success: function(resposta) {
          location.reload();
        }
      });

    });

    function showAlert(seletor, msg, timer = 3000) {
      if (seletor.hasClass("d-none")) {
        seletor.find(".msg").html(msg);
        seletor.removeClass("d-none");
        setTimeout(function() {
          seletor.addClass("d-none");
        }, timer);
      }
    }

    function validoInvalido(seletor, flag) { // flag = true => valido, false => invalido
      if (flag == false) {
        seletor.addClass("is-invalid");
        seletor.removeClass("is-valid");
      } else {
        seletor.removeClass("is-invalid");
        seletor.addClass("is-valid");
      }
    }
  </script>
</body>

</html>