 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
   <!-- Brand Logo -->
   <div class="brand-link">
     <img src="/assets/adminlte/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
     <span class="brand-text font-weight-light">SlingFy&nbsp
       <img class="idioma" idioma="ptb" style="cursor:pointer" src="/assets/imgs/brazil.png" />
       <img class="idioma" idioma="en" style="cursor:pointer" src="/assets/imgs/us.png" />
     </span>
   </div>

   <!-- Sidebar -->
   <div class="sidebar">

     <!-- Sidebar Menu -->
     <nav class="mt-2">
       <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         <li class="nav-item">
           <a href="/afiliados/dashboard" class="nav-link">
             <i class="nav-icon fas fa-tachometer-alt"></i>
             <p><?= $this->lang->line('dashboard') ?></p>
           </a>
         </li>
         <li class="nav-item">
           <a href="/afiliados/convites" class="nav-link">
             <i class="nav-icon fas fa-briefcase"></i>
             <p><?= $this->lang->line('convites') ?></p>
           </a>
         </li>
         <li class="nav-item">
           <a href="/afiliados/lojas" class="nav-link">
             <i class="nav-icon fas fa-shopping-cart"></i>
             <p><?= $this->lang->line('lojas') ?></p>
           </a>
         </li>
         <li class="nav-item">
           <a href="/afiliados/dashboard/configuracoes" class="nav-link">
             <i class="nav-icon fas fa-cog"></i>
             <p><?= $this->lang->line('configuracoesAfiliadoTitulo') ?></p>
           </a>
         </li>
         <li class="nav-item">
           <a href="https://help.slingfy.com/" target="_blank" class="nav-link">
             <i class="nav-icon fas fa-life-ring"></i>
             <p><?= $this->lang->line('ajuda') ?></p>
           </a>
         </li>
         <li class="nav-item">
           <a href="/afiliados/cadastro/logoff" class="nav-link">
             <i class="nav-icon fas fa-sign-out-alt"></i>
             <p><?= $this->lang->line('logout') ?></p>
           </a>
         </li>
       </ul>
     </nav>
     <!-- /.sidebar-menu -->
   </div>
   <!-- /.sidebar -->
 </aside>