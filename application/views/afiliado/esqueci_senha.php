<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Slingfy | <?=$this->lang->line("recuperacaoSenhaTitle")?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="/assets/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/assets/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="../../index2.html"><b>Sling</b>fy</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <div class="alert alert-warning alert-dismissible fade show alertErro d-none" role="alert">
          <span class="msg"></span>
        </div>
        <div class="alert alert-success alert-dismissible fade show alertSucesso d-none" role="alert">
          <span class="msg"></span>
        </div>
        <p><?= $this->lang->line("escolhaSeuIdioma") ?>:&nbsp
          <img class="idioma" idioma="ptb" style="cursor:pointer" src="/assets/imgs/brazil.png" />
          <img class="idioma" idioma="en" style="cursor:pointer" src="/assets/imgs/us.png" />
        </p>

        <p class="login-box-msg"><?= $this->lang->line("recuperacaoSenhaTitle") ?></p>
        <form method="post" class="mt-3">
          <div class="input-group mb-3">
            <input type="email" class="form-control" placeholder="Email" id="email" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-6">

            </div>
            <!-- /.col -->
            <div class="col-6">
              <button type="button" id="recuperarSenha" class="btn btn-primary btn-block"><?= $this->lang->line("recuperarSenha") ?></button>
            </div>
            <!-- /.col -->
          </div>
        </form>

        <p class="mb-0">
          <a href="/afiliados/cadastro/login" class="text-center"><?= $this->lang->line("efetuarLogin") ?></a>
        </p>
      </div>
      <!-- /.login-card-body -->
    </div>
  </div>
  <!-- /.login-box -->

  <!-- jQuery -->
  <script src="/assets/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/assets/adminlte/dist/js/adminlte.min.js"></script>
  <!-- Block Ui -->
  <script src="/assets/blockui/jquery.blockUI.js"></script>

  <script>
    $(document).ready(function() {
      let alertErro = $(".alertErro");
      let alertSucesso = $(".alertSucesso");
      let email = $("#email");

      $(document).ajaxStop($.unblockUI);


      $(".idioma").on("click", function() {
        $.blockUI({
          message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
        });

        let idioma = $(this).attr('idioma');

        $.ajax({
          type: "POST",
          url: "/geral/mudarIdioma",
          data: {
            idioma: idioma
          },
          dataType: "json",
          success: function(resposta) {
            location.reload();
          }
        });

      });

      $("#recuperarSenha").on("click", function() {
        let payload = {
          email: email.val()
        }

        $.blockUI({
          message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
        });
        
        $.ajax({
          type: "POST",
          url: "/afiliados/cadastro/recuperar_senha",
          data: payload,
          dataType: "json",
          success: function(resposta) {
            console.log(resposta);
            if (resposta.success == false) {
              showAlert(alertErro, resposta.msg);
            } else {
              showAlert(alertSucesso, resposta.msg);
            }
          }
        });
      });


    })

    function showAlert(seletor, msg, timer = 3000) {
      if (seletor.hasClass("d-none")) {
        seletor.find(".msg").html(msg);
        seletor.removeClass("d-none");
        setTimeout(function() {
          seletor.addClass("d-none");
        }, timer);
      }
    }
  </script>

</body>

</html>