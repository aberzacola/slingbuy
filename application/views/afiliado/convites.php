<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Slingfy | <?= $this->lang->line('convitesAfiliadosTitulo') ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="/assets/adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Tabulator -->
  <link href="/assets/tabulator/dist/css/tabulator.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php $this->load->view("afiliado/side_bar") ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1><?= $this->lang->line('convitesAfiliadosGerenciamentoConvites') ?></h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="alert alert-warning alert-dismissible fade show alertErro d-none" role="alert">
          <span class="msg"></span>
        </div>
        <div class="alert alert-success alert-dismissible fade show alertSucesso d-none" role="alert">
          <span class="msg"></span>
        </div>
        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"><?= $this->lang->line('convitesAfiliadosConvitesRecebidos') ?></h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div id="convitesRecebidos"></div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.1
      </div>
      <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <div class="modal fade" id="modal-termos">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><?= $this->lang->line("termos") ?></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body modalTermos">
          <p>One fine body&hellip;</p>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line("fechar") ?></button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- jQuery -->
  <script src="/assets/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/assets/adminlte/dist/js/adminlte.min.js"></script>
  <!-- Block Ui -->
  <script src="/assets/blockui/jquery.blockUI.js"></script>
  <!-- Tabulator -->
  <script type="text/javascript" src="/assets/tabulator/dist/js/tabulator.js"></script>
  <script>
    $(document).ajaxStop($.unblockUI);
    $(document).ready(function() {
      let modalTermos = $("#modal-termos");
      let alertErro = $(".alertErro");
      let alertSucesso = $(".alertSucesso");


      var table = new Tabulator("#convitesRecebidos", {
        ajaxURL: "/afiliados/convites/getConvitesAfiliado",
        ajaxConfig: "POST",
        ajaxResponse: function(url, params, response) {
          if (response.success == true) {
            return response.data;
          } else {
            return [];
          }
        },
        layout: "fitColumns",
        columns: [{
            title: "Id",
            field: "id",
            align: "center",
            headerFilter: true
          }, //column has a fixed width of 100px;
          {
            title: "<?= $this->lang->line("loja") ?>",
            field: "nome_loja",
            align: "center",
            headerFilter: true
          }, //column will be allocated 1/5 of the remaining space
          {
            title: "Email",
            field: "email_loja",
            align: "center",
            headerFilter: true
          }, //column will be allocated 3/5 of the remaining space
          {
            title: "<?= $this->lang->line("termos") ?>",
            field: "termos",
            formatter: "html",
            align: "center",
            cellClick: function(e, cell) {
              //e - the click event object
              let row = cell.getRow();
              let rowData = row.getData();
              modalTermos.modal();
              modalTermos.find(".modalTermos").html(rowData.texto_termo);
            }
          },
          {
            title: "Status",
            field: "status",
            formatter: "html",
            align: "center",
            headerFilter: "select",
            headerFilterParams: {
              values: {
                "<?= $this->lang->line("pendente") ?>": "<?= $this->lang->line("pendente") ?>",
                "<?= $this->lang->line("removido") ?>": "<?= $this->lang->line("removido") ?>",
                "<?= $this->lang->line("aceito") ?>": "<?= $this->lang->line("aceito") ?>",
                "<?= $this->lang->line("recusado") ?>": "<?= $this->lang->line("recusado") ?>",
                "<?= $this->lang->line("cancelado") ?>": "<?= $this->lang->line("cancelado") ?>"
              }
            }
          },
          {
            title: "Texto Termo",
            field: "texto_termo",
            visible: false,
            align: "center"
          },
          {
            title: "Aceito",
            field: "aceito",
            visible: false,
            align: "center"
          },
          {
            title: "<?= $this->lang->line("data") ?>",
            field: "data",
            align: "center",
            headerFilter: true,
            sorterParams: {
              format: "<?= $idioma == "ptb" ? "DD/MM/YYYY HH:mm:ss" : "MM/DD/YYYY HH:mm:ss"; ?>",
              alignEmptyValues: "top",
            }
          },
          {
            title: "<?= $this->lang->line("acao") ?>",
            field: "acao",
            formatter: "html",
            align: "center",
            cellClick: function(e, cell) {
              //e - the click event object
              let aceitou = $(e.target).hasClass("aceitar");

              if (!aceitou) {
                var confirmation = confirm("<?= $this->lang->line("certezaQueDesejaCancelar") ?>");

                if (confirmation == false) {
                  return;
                }
              }

              let row = cell.getRow();
              let rowData = row.getData();
              if (rowData.aceito != 0 && rowData.aceito != 1) {
                showAlert(alertErro, "<?= $this->lang->line("convitePendenteErro") ?>");
              } else {
                $.blockUI({
                  message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
                });
                $.ajax({
                  type: "POST",
                  url: "/afiliados/convites/decisaoConvite",
                  data: {
                    id_convite: rowData.id,
                    aceitou: aceitou
                  },
                  dataType: "json",
                  success: function(resposta) {
                    if (resposta.success == false) {
                      showAlert(alertErro, resposta.msg, 10000);
                    } else {
                      showAlert(alertSucesso, resposta.msg, 10000);
                      table.setData();
                    }
                  }
                });
              }
            }
          }
        ]
      });

      $(".idioma").on("click", function() {
        $.blockUI({
          message: '<div class="spinner-border mt-3" role="status"><span class="sr-only">Loading...</span></div> <p><?= $this->lang->line("processando") ?></p>'
        });

        let idioma = $(this).attr('idioma');

        $.ajax({
          type: "POST",
          url: "/geral/mudarIdioma",
          data: {
            idioma: idioma
          },
          dataType: "json",
          success: function(resposta) {
            location.reload();
          }
        });

      });
    });

    function showAlert(seletor, msg, timer = 3000) {
      if (seletor.hasClass("d-none")) {
        seletor.find(".msg").html(msg);
        seletor.removeClass("d-none");
        $("html, body").animate({
          scrollTop: 0
        }, "slow");
        setTimeout(function() {
          seletor.addClass("d-none");
        }, timer);
      }
    }
  </script>
</body>

</html>