<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Hashids\Hashids;

class Geral extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == 'OPTIONS') {
            die();
        }
    }

    public function mudarIdioma()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['idioma'] = $this->input->post("idioma");
        $shop = $this->input->post("shop");

        if (isset($_SESSION['id_usuario'])) {
            $this->load->model("Usuarios_model", "", true);
            $update = array(
                "idioma" => $this->input->post("idioma")
            );
            $this->Usuarios_model->updateUsuario($_SESSION['id_usuario'], $update);
        } else if ($shop) {
            $this->load->model("Lojas_model", "", true);
            $loja = $this->Lojas_model->get_loja_shopify_by_name($shop);
            $update['idioma'] = $this->input->post("idioma");
            $this->Lojas_model->update_loja($loja['id'], $update);
        }

        ajax(true, "", "");
    }

    public function contadorVisitas()
    {

        $hash = $this->input->post("hash");
        $primeira = $this->input->post("primeira");

        $hashids = new Hashids($this->config->item("id_salt"));
        $ids = $hashids->decode($hash); // 0 => convite_id, 1 => usuario_id, 2 => id_loja
        if (empty($ids)) {
            die;
        }

        $this->load->model("Visitas_model", "", true);

        $id_afiliado = $ids[0];
        $id_usuario = $ids[1];
        $id_loja = $ids[2];

        $this->Visitas_model->inserirVisita($id_loja, $id_usuario, $id_afiliado, $primeira);
    }

    public function teste($id)
    {
        $this->load->model("Usuarios_model", "", true);

        $asd = $this->Usuarios_model->getUsuario(12);

        var_dump($asd);
    }

    public function updateWebHooksAndScriptTags()
    {

        $this->load->library("shopify");
        $shop_domain = "devmcp.myshopify.com";
        $loja = $this->Lojas_model->get_loja_shopify_by_name($shop_domain);

        $script_tags = $this->shopify->get_checkout_info($shop_domain, $loja['access_token'] );
        $script_tags = json_decode($script_tags, true);
        foreach ($script_tags['script_tags'] as $tag) {
            $this->shopify->turn_off_checkout($shop_domain, $loja['access_token'], $tag['id']);
        }

        $webhooks = $this->shopify->get_webhooks($shop_domain, $loja['access_token']);
        $webhooks = json_decode($webhooks, true);

        foreach ($webhooks['webhooks'] as $hooks) {
            $this->shopify->delete_webhooks($shop_domain, $loja['access_token'], $hooks['id']);
        }

        $this->shopify->turn_on_checkout($shop_domain, $loja['access_token']);
        $this->shopify->create_webhooks_order_create($shop_domain, $loja['access_token']);
        $this->shopify->create_webhooks_order_update($shop_domain, $loja['access_token']);
    }
}
