<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Crons extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function processar_fila_orders()
    {
        $this->load->model("Fila_model", "", true);
        $this->load->model("Afiliados_model", "", true);
        $this->load->model("Usuarios_model", "", true);
        $this->load->model("Lojas_model", "", true);
        $this->load->model("Vendas_model", "", true);
        $this->load->model("Pagamentos_model", "", true);
        $this->load->library("Shopify");

        $orders = $this->Fila_model->getAllOrders();
        
        if($orders == false){
            die;
        }

        
        while ($atual = current($orders)) {
            $order = json_decode($atual['order'], true);
            
            $venda = $this->Vendas_model->getVendaByOrderId($order['id']);

            if ($venda == false) {
                $this->Fila_model->deleteOrder($atual['id']);
                next($orders);
                continue;
            } else {
                $id_afiliado = $venda['id_afiliado']; // AKA id convite
                $id_usuario = $venda['id_usuario'];
                $id_loja = $venda['id_loja'];
            }
            
            $dados_loja = $this->Lojas_model->get_loja_shopify_by_id($id_loja);

            if($dados_loja == false){
                $this->Fila_model->deleteOrder($atual['id']);
                next($orders);
                continue;
            }


            if($venda['pago'] != $order['financial_status']){
                $this->Vendas_model->updateStatusPagamento($venda['id'], $order['financial_status']);
            }

            $convite = $this->Afiliados_model->getConvite($id_afiliado);

            if ($convite['aceito'] != 1 || $convite == false) {
                $update_order = array(
                    "order" => array(
                        "id" => $order['id'],
                        "note_attributes" => array(
                            array(
                                "name" => "Slingfy",
                                "value" => $this->lang->line('afiliadoNaoAceitouConvite')
                            )
                        )
                    )
                );


                $this->shopify->update_order($dados_loja['dominio_shopify'], $dados_loja['access_token'], $update_order, $order['id']);
                $this->Fila_model->deleteOrder($atual['id']);
                next($orders);
                continue;
            }

            $recorrencia_atual = $this->Pagamentos_model->getLastRecorrenciaByIdLoja($dados_loja['id']);
            
            if ($recorrencia_atual == false) {
                next($orders);
                continue;
            } else {
                if ($recorrencia_atual['status'] != "active") {
                    next($orders);
                    continue;
                }
                if (($recorrencia_atual['limite_restante'] / 100)  < ($order['total_price_usd'] * $this->config->item("porcentagem_cobranca"))) {
                    next($orders);
                    continue;
                }
            }
            
            if ($dados_loja['idioma'] == "ptb") {
                $this->lang->load('app_lang', 'ptb');
            } else {
                $this->lang->load('app_lang', 'en');
            }

            $msg_note = $this->lang->line("notasAdicionais");
            $msg_note = str_ireplace("{nomeUsuario}", $convite['nome_afiliado'], $msg_note);
            $msg_note = str_ireplace("{id_convite}", $id_afiliado, $msg_note);

            $update_order = array(
                "order" => array(
                    "id" => $order['id'],
                    "note_attributes" => array(
                        array(
                            "name" => "Slingfy",
                            "value" => $msg_note
                        )
                    ),
                    "tags" => "Slingfy, Slingfy_$id_afiliado"
                )
            );


            $this->shopify->update_order($dados_loja['dominio_shopify'], $dados_loja['access_token'], $update_order, $order['id']);

            $venda = $this->Vendas_model->getVendaByOrderId($order['id']);


            if ($order['financial_status'] == "paid" && $venda['cobrado'] == 0  ) {
                $descricao = $this->lang->line('extrato');
                $descricao = str_ireplace("{orderId}", $order['id'], $descricao);
                $descricao = str_ireplace("{conviteId}", $convite['id'], $descricao);
                $valor = $order['total_price_usd'] * $this->config->item("porcentagem_cobranca");
                $this->shopify->create_usage_charge($dados_loja['dominio_shopify'], $dados_loja['access_token'], $recorrencia_atual['id_recorrencia_shopify'], $descricao, $valor);
                $this->Vendas_model->updateCobrancaShopifyFeita($venda['id'], 1);
            }

            $resultado = $this->shopify->get_recurring_charge($dados_loja['dominio_shopify'], $dados_loja['access_token'], $recorrencia_atual["id_recorrencia_shopify"]);
            $parsed = json_decode($resultado, true);

            $recorrencia['dias_de_teste'] = $parsed['recurring_application_charge']['trial_days'];
            $recorrencia['dias_de_teste_fim'] = strtotime($parsed['recurring_application_charge']['trial_ends_on']);
            $recorrencia['status'] = $parsed['recurring_application_charge']['status'];
            $recorrencia['limite'] = floatval($parsed['recurring_application_charge']['capped_amount']) * 100;
            $recorrencia['limite_utilizado'] = $parsed['recurring_application_charge']['balance_used'] * 100;
            $recorrencia['limite_restante'] = $parsed['recurring_application_charge']['balance_remaining'] * 100;

            $this->Pagamentos_model->updateRecorrencia($recorrencia_atual['id'], $recorrencia);

            $this->Fila_model->deleteOrder($atual['id']);

            next($orders);
        }
    }
}
