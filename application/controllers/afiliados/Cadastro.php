<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cadastro extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('afiliado/registrar');
	}

	public function cadastra()
	{
		$this->load->model("Usuarios_model", "", true);
		$this->load->library("sendemail");

		$payload = $this->input->post();

		$email = $payload['email'];
		$nome = $payload['nome'];
		$resenha = $payload['resenha'];
		$senha = $payload['senha'];
		$idioma = $this->lang->get_current_lang();
		$ativo = 0;

		if ($this->Usuarios_model->verificaEmail($email)) {
			ajax(false, $this->lang->line("emailJaCadastrado"), "");
			return false;
		}

		if ($senha != $resenha) {
			ajax(false, $this->lang->line("senhasNaoSaoIguais"), "");
			return false;
		}

		if (preg_match("/^[a-z]([-']?[a-z]+)*( [a-z]([-']?[a-z]+)*)+$/i", $nome) == false) {
			ajax(false, $this->lang->line("nomeInvalido"), "");
			return false;
		}

		if (strlen($senha) < 6) {
			ajax(false, $this->lang->line("dicasSenha"), "");
			return false;
		}


		try {
			$id_usuario = $this->Usuarios_model->guardaCadastro($nome, $email, $senha, $ativo, $idioma);
			$usuario = $this->Usuarios_model->getUsuario($id_usuario);

			$hashVerificador = md5($id_usuario . $email . $usuario['dt_cadastro'] . $this->config->item("md5_salt"));

			//Colocando o ID no meio do HASH
			$newHash = "";
			for ($i = 0; $i < strlen($id_usuario); $i++) {
				$newHash .= substr($hashVerificador, $i, 1);
				$newHash .= substr($id_usuario, $i, 1);
			}
			$newHash .= ".";
			$newHash .= substr($hashVerificador, $i);

			$data['email_preview_text'] = $this->lang->line("previewCadastroAfiliaddo");
			$data['email_msg'] = str_ireplace("{nome_afiliado}", $nome, $this->lang->line("msgCadastroAfiliado"));
			$data['link_confirmacao'] = $this->config->item("base_url") . "afiliados/cadastro/validaEmail/" . $newHash;
			$data['texto_confirmacao'] = $this->lang->line("msgCadastroConfirmar");
			$data['email_msg_bottom'] = $this->lang->line("msgCadastroFooter");
			$assunto = $this->lang->line("assuntoCadastroAfiliado");


			$corpo_email = $this->load->view("emails/verificar_email", $data, true);

			$this->sendemail->enviar($email, $nome, $assunto, $corpo_email);

			ajax(true, $this->lang->line("cadastroEfetuadoComSucessoEmail"), "");
			return false;
		} catch (Exception $e) {
			ajax(false, $this->lang->line("erroGeral"), "");
			return false;
		}
	}

	public function alterarSenha()
	{
		$this->load->model("Usuarios_model", "", true);

		$senhaAtual = $this->input->post("senhaAtual");
		$novaSenha = $this->input->post("novaSenha");

		$usuario = $this->Usuarios_model->getUsuario($_SESSION['id_usuario']);

		if (password_verify($senhaAtual, $usuario['senha'])) {

			$update['senha'] = $novaSenha;

			$this->Usuarios_model->updateUsuario($usuario['id'], $update);
			ajax(true, $this->lang->line("configuracoesAfiliadoSenhaAlteradaSucesso"), "");
			return false;
		} else {
			ajax(false, $this->lang->line("configuracoesAfiliadoSenhaIncorreta"), "");
			return false;
		}
	}

	public function validaEmail($hashVerificacao)
	{
		$this->load->model("Usuarios_model", "", true);

		//Extraindo o ID no meio do HASH
		$id_hashado = "";
		$hashOriginal = "";
		$fim = false;
		for ($i = 0; $i < strlen($hashVerificacao); $i++) {
			if ($i % 2 != 0 && $fim == false) {
				$id_hashado .= substr($hashVerificacao, $i, 1);
				if (substr($hashVerificacao, $i + 1, 1) == '.') {
					$fim = true;
					$i++;
				}
			} else {
				$hashOriginal .= substr($hashVerificacao, $i, 1);
			}
		}

		$usuario = $this->Usuarios_model->getUsuario($id_hashado);

		if ($usuario !== false) {
			$hashVerificador = md5($usuario['id'] . $usuario['email'] . $usuario['dt_cadastro'] . $this->config->item("md5_salt"));

			if ($hashVerificador == $hashOriginal) {
				$update = array(
					"ativo" => 1
				);
				$this->Usuarios_model->updateUsuario($id_hashado, $update);
				$_SESSION['id_usuario'] = $id_hashado;

				$data['nome_usuario'] = $usuario['nome'];
				$data['tipo_alerta'] = "success";
				$data['msg'] = $this->lang->line("emailVerificado");
			} else {
				$data['nome_usuario'] = "?";
				$data['tipo_alerta'] = "warning";
				$data['msg'] = $this->lang->line("falhaVerificacaoEmail");
			}
		} else {
			$data['nome_usuario'] = "?";
			$data['tipo_alerta'] = "warning";
			$data['msg'] = $this->lang->line("falhaVerificacaoEmail");
		}

		$this->load->view("afiliado/verificado", $data);
	}

	public function esqueci_senha()
	{
		$this->load->view("afiliado/esqueci_senha");
	}

	public function recuperar_senha()
	{

		$this->load->model("Usuarios_model", "", true);
		$this->load->library("sendemail");

		$email = $this->input->post("email");

		if ($this->Usuarios_model->verificaEmail($email) == false) {
			ajax(false, $this->lang->line("emailNaoEncontrado"), "");
			return false;
		} else {
			$usuario = $this->Usuarios_model->getUserByEmail($email);

			$hashVerificador = md5($usuario['id'] . $email . $usuario['dt_update'] . $this->config->item("md5_salt"));

			//Colocando o ID no meio do HASH
			$newHash = "";
			for ($i = 0; $i < strlen($usuario['id']); $i++) {
				$newHash .= substr($hashVerificador, $i, 1);
				$newHash .= substr($usuario['id'], $i, 1);
			}
			$newHash .= ".";
			$newHash .= substr($hashVerificador, $i);


			$data['email_preview_text'] = $this->lang->line("previewRecuperaSenhaAfiliado");
			$data['email_msg'] = $this->lang->line("msgRecuperarSenhaAfiliado");
			$data['link_confirmacao'] = $this->config->item("base_url") . "afiliados/cadastro/redefinir_senha/" . $newHash;
			$data['texto_confirmacao'] = $this->lang->line("msgRecuperarSenhaAfiliadoButton");
			$data['email_msg_bottom'] = $this->lang->line("msgRecuperarSenhaAfiliadoFooter");
			$assunto = $this->lang->line("assuntoRecuperarSenhaAfiliado");

			$corpo_email = $this->load->view("emails/recuperar_senha", $data, true);

			$this->sendemail->enviar($email, $usuario['nome'], $assunto, $corpo_email);

			ajax(true, $this->lang->line("emailRecuperacaoSenhaEnviado"), "");
			return false;
		}
	}

	public function redefinir_senha($hashVerificacao)
	{
		$this->load->model("Usuarios_model", "", true);

		//Extraindo o ID no meio do HASH
		$id_hashado = "";
		$hashOriginal = "";
		$fim = false;
		for ($i = 0; $i < strlen($hashVerificacao); $i++) {
			if ($i % 2 != 0 && $fim == false) {
				$id_hashado .= substr($hashVerificacao, $i, 1);
				if (substr($hashVerificacao, $i + 1, 1) == '.') {
					$fim = true;
					$i++;
				}
			} else {
				$hashOriginal .= substr($hashVerificacao, $i, 1);
			}
		}

		$usuario = $this->Usuarios_model->getUsuario($id_hashado);

		if ($usuario !== false) {
			$hashVerificador = md5($usuario['id'] . $usuario['email'] . $usuario['dt_update'] . $this->config->item("md5_salt"));

			if ($hashVerificador == $hashOriginal) {
				$senha = substr(md5(rand(0, 99999999) . $this->config->item('md5_salt')), 0, 16);

				$update = array(
					"senha" => $senha
				);
				$this->Usuarios_model->updateUsuario($id_hashado, $update);

				$data['nome_usuario'] = $usuario['nome'];
				$data['tipo_alerta'] = "success";
				$data['msg'] = str_ireplace("{senha}", $senha, $this->lang->line("suaNovaSenhaE"));
			} else {
				$data['nome_usuario'] = "?";
				$data['tipo_alerta'] = "warning";
				$data['msg'] = $this->lang->line("falhaVerificacaoNovaSenha");
			}
		} else {
			$data['nome_usuario'] = "?";
			$data['tipo_alerta'] = "warning";
			$data['msg'] = $this->lang->line("falhaVerificacaoNovaSenha");
		}

		$this->load->view("afiliado/nova_senha", $data);
	}

	public function efetivaLogin()
	{

		$this->load->model("Usuarios_model", "", true);

		$email = $this->input->post("email");
		$senha = $this->input->post("senha");

		$usuario = $this->Usuarios_model->getUserByEmail($email);


		if (password_verify($senha, $usuario['senha'])) {
			if ($usuario['ativo'] == 0) {
				ajax(false, $this->lang->line("assuntoCadastroAfiliado"), "");
				return false;
			}
			$_SESSION['id_usuario'] = $usuario['id'];
			ajax(true, $this->lang->line("logadoComSucesso"), "");
			return false;
		} else {
			ajax(false, $this->lang->line("logadoErro"), "");
			return false;
		}
	}

	public function login($flag = 0)
	{
		
		$data['msg'] = "n";
		if ($flag != 0) {
			$data['msg'] = $this->lang->line("errorLogin");
		}
		$this->load->view('afiliado/login', $data);
	}

	public function logoff()
	{
		$this->load->helper('url');
		unset($_SESSION['id_usuario']);


		redirect('afiliados/cadastro/login');
	}
}
