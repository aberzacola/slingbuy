<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Hashids\Hashids;

class Lojas extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model("Afiliados_model", "", true);
		$this->load->model("Usuarios_model", "", true);
		$this->load->model("Lojas_model", "", true);
		$this->load->library("math");
		$this->load->library("shopify");

		$usuario = $this->Usuarios_model->getUsuario($_SESSION['id_usuario']);

		$convites = $this->Afiliados_model->getTodosConvitesAceitosAfiliado($usuario['email']);

		$convites_pendentes = $this->Afiliados_model->getTodosConvitesPendentesAfiliado($usuario['email']);
		
		if($convites_pendentes != false){
			$data['convites_pendentes'] = true;
		}else{
			$data['convites_pendentes'] = false;
		}

		if (!empty($convites)) {
			foreach ($convites as $key => $convite) {
				$hashids = new Hashids($this->config->item("id_salt"));
				$id = $hashids->encode($convite['id'], $usuario['id'], $convite['id_loja']);

				$convites[$key]['hash'] = $id;

				$dados_loja = $this->Lojas_model->get_loja_shopify_by_id($convite['id_loja']);
				$produtos = $this->shopify->get_all_active_products($dados_loja['dominio_shopify'], $dados_loja['access_token']);
				$produtos = json_decode($produtos, true);
				foreach ($produtos['products'] as $key2 => $produto) {
					$convites[$key]['produtos'][$key2]['nome'] = $produto['title'];
					$convites[$key]['produtos'][$key2]['url'] = "https://" . $dados_loja['dominio_proprio'] . "/products/" . $produto['handle'] . "?slgf=" . $id;
					foreach ($produto['images'] as $key3 => $images) {
						$convites[$key]['produtos'][$key2]['images'][$key3]['src'] = $images['src'];
						$convites[$key]['produtos'][$key2]['images'][$key3]['alt'] = $images['alt'] != "" ?: $this->lang->line('imagem') . " - " . $produto['title'];
					}
				}
			}
			$data['convites_aceitos'] = $convites;
		}else{
			$data['convites_aceitos'] = false;
		}


		
		$this->load->view("afiliado/lojas", $data);
	}
}
