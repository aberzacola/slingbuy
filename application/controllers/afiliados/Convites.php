<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Convites extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model("Usuarios_model", "", TRUE);

		$dados_usuarios = $this->Usuarios_model->getUsuario($_SESSION['id_usuario']);

		$this->load->view("afiliado/convites", $dados_usuarios);
	}

	public function getConvitesAfiliado()
	{
		$this->load->model("Afiliados_model", "", TRUE);
		$this->load->model("Usuarios_model", "", TRUE);

		$usuario = $this->Usuarios_model->getUsuario($_SESSION['id_usuario']);

		$convites = $this->Afiliados_model->getTodosConvitesAfiliado($usuario['email']);


		$tabela_afiliados = array();
		if($convites != false)
		foreach ($convites as $key => $convite) {
			$tabela_afiliados[$key]['id'] = $convite['id'];
			$tabela_afiliados[$key]['nome_loja'] = $convite['nome_loja'];
			$tabela_afiliados[$key]['email_loja'] = $convite['email_lojista'];
			$tabela_afiliados[$key]['termos']  = '<i class="nav-icon fas fa-file"></i>';
			$tabela_afiliados[$key]['texto_termo'] = $convite['termos'];

			$tabela_afiliados[$key]['acao'] = '-';

			$tabela_afiliados[$key]['aceito'] = $convite['aceito'];
			$data_envio = strtotime($convite['data_envio']);

			if ($usuario['idioma'] == "ptb") {
				$tabela_afiliados[$key]['data'] = date('d/m/Y H:i:s', $data_envio);
			} else {
				$tabela_afiliados[$key]['data'] = date('m/d/Y H:i:s', $data_envio);
			}

			switch ($convite['aceito']) {
				case 0:
					$tabela_afiliados[$key]['status'] = $this->lang->line("pendente");
					$tabela_afiliados[$key]['acao'] = '
					<button type="button" class="btn btn-block btn-success aceitar">' . $this->lang->line('aceitar') . '</button>
					<button type="button" class="btn btn-block btn-danger recusar">' . $this->lang->line('recusar') . '</button>';
					break;
				case 1:
					$tabela_afiliados[$key]['status'] = $this->lang->line("aceito");
					$tabela_afiliados[$key]['acao'] = '<button type="button" class="btn btn-block btn-warning recusar">' . $this->lang->line('cancelar') . '</button>';
					break;
				case -1:
					$tabela_afiliados[$key]['status'] = $this->lang->line("recusado");
					break;
				case -2:
					$tabela_afiliados[$key]['status'] = $this->lang->line("removido");
					break;
				case -3:
					$tabela_afiliados[$key]['status'] = $this->lang->line("cancelado");
					break;
			}
		}

		ajax(true, "", $tabela_afiliados);
	}

	public function decisaoConvite()
	{
		$this->load->model("Afiliados_model", "", true);
		$this->load->model("Lojas_model", "", true);
		$this->load->library('sendemail');


		$id_convite = $this->input->post("id_convite");
		$aceitou = $this->input->post("aceitou");

		$aceitou = ($aceitou == "false") ? false : true;

		$convite = $this->Afiliados_model->getConvite($id_convite);
		$lojas = $this->Lojas_model->get_loja_shopify_by_id($convite['id_loja']);

		if ($aceitou) {
			$resposta = $this->Afiliados_model->aceitaConvite($id_convite);
			$msg = $this->lang->line("conviteAceitoAfiliado");
		} elseif($convite['aceito'] == 1) {
			$resposta = $this->Afiliados_model->suspenderConvite($id_convite);
			$msg = $this->lang->line("conviteSuspensoComSucesso");
		}else{
			$resposta = $this->Afiliados_model->recusaConvite($id_convite);
			$msg = $this->lang->line("conviteRecusadoAfiliado");
		}

		$this->lang->load('app_lang', $lojas['idioma']);
		if ($convite['aceito'] == 1) {
			//Construindo o email
			$data['email_preview_text']  = $this->lang->line("previewConviteSuspensoLojista");
			$assunto  = $this->lang->line("assuntoConviteSuspensoLojista");
			$assunto  = str_ireplace("{nome_afiliado}", $convite['nome_afiliado'], $assunto);

			$data['email_msg']  = $this->lang->line("msgAfiliacaoSuspensaLojista");
			$data['email_msg'] = str_ireplace("{nome_loja}", $convite['nome_loja'], $data['email_msg']);
			$data['email_msg'] = str_ireplace("{nome_afiliado}", $convite['nome_afiliado'], $data['email_msg']);
			$data['link_convite'] = "#";
			$data['convite'] = $this->lang->line("msgAfiliacaoSuspensaLojistaBotao");
			$data['email_msg_bottom'] = $this->lang->line("msgAfiliacaoSuspensaLojistaFooter");

			$corpo_email = $this->load->view("emails/convite_afiliado", $data, true);
		} else {
			if ($aceitou) {
				//Construindo o email
				$data['email_preview_text']  = $this->lang->line("previewConviteAceito");
				$assunto  = $this->lang->line("assuntoConviteAceito");
				$assunto  = str_ireplace("{nome_afiliado}", $convite['nome_afiliado'], $assunto);

				$data['email_msg']  = $this->lang->line("msgConviteAfiliadoAceito");
				$data['email_msg'] = str_ireplace("{nome_lojista}", $convite['nome_loja'], $data['email_msg']);
				$data['email_msg'] = str_ireplace("{nome_afiliado}", $convite['nome_afiliado'], $data['email_msg']);
				$data['link_convite'] = "#";
				$data['convite'] = $this->lang->line("msgConviteAfiliadoAceitoBotao");
				$data['email_msg_bottom'] = $this->lang->line("msgConviteAfiliadoAceitoFooter");

				$corpo_email = $this->load->view("emails/convite_afiliado", $data, true);
			} else {
				$data['email_preview_text']  = $this->lang->line("previewConviteRecusado");
				$assunto  = $this->lang->line("assuntoConviteRecusado");
				$assunto  = str_ireplace("{nome_afiliado}", $convite['nome_afiliado'], $assunto);

				$data['email_msg']  = $this->lang->line("msgConviteAfiliadoRecusado");
				$data['email_msg'] = str_ireplace("{nome_lojista}", $convite['nome_loja'], $data['email_msg']);
				$data['email_msg'] = str_ireplace("{nome_afiliado}", $convite['nome_afiliado'], $data['email_msg']);
				$data['link_convite'] = "#";
				$data['convite'] = $this->lang->line("msgConviteAfiliadoRecusadoBotao");
				$data['email_msg_bottom'] = $this->lang->line("msgConviteAfiliadoRecusadoFooter");

				$corpo_email = $this->load->view("emails/convite_afiliado", $data, true);
			}
		}
		$this->lang->load('app_lang', $_SESSION['idioma']);


		$this->sendemail->enviar($convite['email_lojista'], $convite['nome_loja'], $assunto, $corpo_email);

		if ($resposta) {
			ajax(true, $msg, "");
		} else {
			ajax(false, $this->lang->line('erroGeral'), "");
		}
	}
}
