<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Dashboard extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model("Afiliados_model", "", true);
		$this->load->model("Usuarios_model", "", true);

		$usuario = $this->Usuarios_model->getUsuario($_SESSION['id_usuario']);
		$convites = $this->Afiliados_model->getTodosConvitesAfiliado($usuario['email']);

		$data['convites'] = $convites;

		$this->load->view("afiliado/dashboard", $data);
	}

	public function ordersXLSX()
	{

		$this->load->model("Estatisticas_model", "", true);

		$id_convite = $this->input->post("id_convite", true);


		if ($id_convite == 0) {
			$resposta = $this->Estatisticas_model->getAllVendasByUsuario($_SESSION['id_usuario']);
		} else {
			$resposta = $this->Estatisticas_model->getAllVendasByUsuarioEConvite($_SESSION['id_usuario'], $id_convite);
		}

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', $this->lang->line("todasVendasRelatorio"));
		$sheet->setCellValue('A2', $this->lang->line("correcaoGMT"));
		$sheet->setCellValue('B2', $_SESSION['idioma'] == "ptb" ? "-3" : "0");

		$start_line = 3;
		$sheet->setCellValue('A' . $start_line, $this->lang->line("idConvite"));
		$sheet->setCellValue('B' . $start_line, $this->lang->line("orderId"));
		$sheet->setCellValue('C' . $start_line, $this->lang->line("valorTotal"));
		$sheet->setCellValue('D' . $start_line, $this->lang->line("statusPagamento"));
		$sheet->setCellValue('E' . $start_line, $this->lang->line("nomeLoja"));
		$sheet->setCellValue('F' . $start_line, $this->lang->line("dataCriada"));
		$sheet->setCellValue('G' . $start_line, $this->lang->line("dataCriadaGMT"));
		$sheet->setCellValue('H' . $start_line, $this->lang->line("dataAtualizada"));
		$sheet->setCellValue('I' . $start_line, $this->lang->line("dataAtualizadaGMT"));

		if ($resposta)
			foreach ($resposta as $key => $linha) {
				$celNumber = ($key + $start_line + 1);
				$sheet->setCellValue('A' . $celNumber, $linha['id_afiliado']);
				$sheet->setCellValue('B' . $celNumber, $linha['order_id']);
				$sheet->setCellValue('C' . $celNumber, $linha['valor'] / 100);
				$sheet->setCellValue('D' . $celNumber, $linha['pago']);
				$sheet->setCellValue('E' . $celNumber, $linha['nome_loja']);

				$excelDateValue = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($linha['dt_created_unix']);

				$sheet->setCellValue('F' . $celNumber, $excelDateValue);
				$spreadsheet->getActiveSheet()->getStyle('F' . $celNumber)
					->getNumberFormat()
					->setFormatCode(
						\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME
					);
				$sheet->setCellValue('G' . $celNumber, "=F$celNumber+(B2/24)");
				$spreadsheet->getActiveSheet()->getStyle('G' . $celNumber)
					->getNumberFormat()
					->setFormatCode(
						\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME
					);

				$excelDateValue = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($linha['dt_updated_unix']);

				$sheet->setCellValue('H' . $celNumber, $excelDateValue);
				$spreadsheet->getActiveSheet()->getStyle('H' . $celNumber)
					->getNumberFormat()
					->setFormatCode(
						\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME
					);
				$sheet->setCellValue('I' . $celNumber, "=H$celNumber+(B2/24)");
				$spreadsheet->getActiveSheet()->getStyle('I' . $celNumber)
					->getNumberFormat()
					->setFormatCode(
						\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME
					);
			}
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="orders.xlsx"');
		header('Cache-Control: max-age=0');
		$writer = new Xlsx($spreadsheet);
		$writer->save('php://output');
	}

	public function visitasXLSX()
	{

		$this->load->model("Estatisticas_model", "", true);

		$id_convite = $this->input->post("id_convite", true);


		if ($id_convite == 0) {
			$resposta = $this->Estatisticas_model->getAllVisitasByUsuario($_SESSION['id_usuario']);
		} else {
			$resposta = $this->Estatisticas_model->getAllVisitasByUsuarioEConvite($_SESSION['id_usuario'], $id_convite);
		}

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', $this->lang->line("todasVisitasRelatorio"));
		$sheet->setCellValue('A2', $this->lang->line("correcaoGMT"));
		$sheet->setCellValue('B2', $_SESSION['idioma'] == "ptb" ? "-3" : "0");

		$start_line = 3;
		$sheet->setCellValue('A' . $start_line, "id");
		$sheet->setCellValue('B' . $start_line, $this->lang->line("nomeLoja"));
		$sheet->setCellValue('C' . $start_line, $this->lang->line("tipoVisita"));
		$sheet->setCellValue('D' . $start_line, $this->lang->line("dataCriada"));
		$sheet->setCellValue('E' . $start_line, $this->lang->line("dataCriadaGMT"));

		if ($resposta)
			foreach ($resposta as $key => $linha) {
				$celNumber = ($key + $start_line + 1);
				$sheet->setCellValue('A' . $celNumber, $linha['id']);
				$sheet->setCellValue('B' . $celNumber, $linha['nome']);

				$visita = $linha['primeira'] == 1 ? $this->lang->line('unica') : $this->lang->line('retorno');

				$sheet->setCellValue('C' . $celNumber, $visita);

				$excelDateValue = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($linha['dt_created_unix']);

				$sheet->setCellValue('D' . $celNumber, $excelDateValue);
				$spreadsheet->getActiveSheet()->getStyle('D' . $celNumber)
					->getNumberFormat()
					->setFormatCode(
						\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME
					);
				$sheet->setCellValue('E' . $celNumber, "=D$celNumber+(B2/24)");
				$spreadsheet->getActiveSheet()->getStyle('E' . $celNumber)
					->getNumberFormat()
					->setFormatCode(
						\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME
					);
			}
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="visits.xlsx"');
		header('Cache-Control: max-age=0');
		$writer = new Xlsx($spreadsheet);
		$writer->save('php://output');
	}

	public function getAllVisitasChart()
	{
		$this->load->model("Estatisticas_model", "", true);

		$id_usuario = $_SESSION['id_usuario'];

		$visitas = $this->Estatisticas_model->getAllVisitasByUsuario($id_usuario);

		$grafico = array();
		foreach ($visitas as $key => $visita) {
			switch ($visita['primeira']) {
				case 1:
					if (!isset($grafico[$this->lang->line('unicas')])) {
						$grafico[$this->lang->line('unicas')] = 0;
					}
					$grafico[$this->lang->line('unicas')]++;
					break;
				case 0:
					if (!isset($grafico[$this->lang->line('retornos')])) {
						$grafico[$this->lang->line('retornos')] = 0;
					}
					$grafico[$this->lang->line('retornos')]++;
					break;
			}
		}

		ajax(true, "", $grafico);
	}

	public function getAllVendasChart()
	{
		$this->load->model("Estatisticas_model", "", true);

		$id_usuario = $_SESSION['id_usuario'];

		$vendas = $this->Estatisticas_model->getAllVendasByUsuario($id_usuario);

		$grafico = array();
		foreach ($vendas as $key => $venda) {
			if (isset($grafico[$venda['pago']])) {
				$grafico[$venda['pago']] += $venda['valor'] / 100;
			} else {
				$grafico[$venda['pago']] = $venda['valor'] / 100;
			}
		}


		ajax(true, "", $grafico);
	}

	public function configuracoes()
	{
		$this->lang->get_current_lang();
		$this->load->view("afiliado/configuracoes");
	}
}
