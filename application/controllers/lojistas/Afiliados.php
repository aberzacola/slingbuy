<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Afiliados extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   *	- or -
   * 		http://example.com/index.php/welcome/index
   *	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */
  public function index()
  {
    $this->load->library("shopify");
    $this->load->model("Lojas_model", "", true);

    $dominio_shopify = $this->input->get_post("shop");
    $dados_loja = $this->Lojas_model->get_loja_shopify_by_name($dominio_shopify);


    $this->load->view("lojas/afiliados", $dados_loja);
  }

  public function convidaAfiliado()
  {
    $this->load->model("Afiliados_model", "", true);
    $this->load->model("Lojas_model", "", true);
    $this->load->library("sendemail");

    $dominio_shopify = $this->input->get_post("shop");
    $dados_loja = $this->Lojas_model->get_loja_shopify_by_name($dominio_shopify);

    $id_loja = $dados_loja['id'];
    $nome_afiliado = $this->input->post("nomeAfilado");
    $email_afiliado = $this->input->post("emailAfiliado");
    $termos = $this->input->post("termosAfiliado");
    $aceito = 0;

    $resposta = $this->Afiliados_model->inserirConvite($id_loja, $nome_afiliado, $email_afiliado, $termos, $aceito);


    //Construindo o email
    $data['email_preview_text']  = $this->lang->line("previewConviteAfiliado");
    $assunto  = $this->lang->line("assuntoConviteAfiliado");
    $data['email_msg']  = $this->lang->line("msgConviteAfiliado");
    $data['email_msg'] = str_ireplace("{nome_afiliado}", $nome_afiliado, $data['email_msg']);
    $data['email_msg'] = str_ireplace("{nome_loja}", $dados_loja['nome'], $data['email_msg']);
    $data['link_convite'] = $this->config->item("base_url");
    $data['convite'] = $this->lang->line("msgConviteAfiliadoBotao");
    $data['email_msg_bottom'] = $this->lang->line("msgConviteAfiliadoFooter");

    $corpo_email = $this->load->view("emails/convite_afiliado", $data, true);

    $this->sendemail->enviar($email_afiliado, $nome_afiliado, $assunto, $corpo_email);

    if (is_int($resposta)) {
      ajax(true, $this->lang->line('conviteEnviadoComSucesso'), "");
    } else {
      ajax(false, $this->lang->line('erroGeral'), "");
    }
  }

  public function getTabelaAfiliados()
  {
    $this->load->model("Afiliados_model", "", true);
    $this->load->model("Lojas_model", "", true);

    $dominio_shopify = $this->input->get_post("shop");
    $dados_loja = $this->Lojas_model->get_loja_shopify_by_name($dominio_shopify);


    $todos_convites = $this->Afiliados_model->getTodosConvitesLojista($dados_loja['id']);

    $dados_tabela = array();
    if ($todos_convites != false) {
      foreach ($todos_convites as $key => $convites) {
        $dados_tabela[$key]['id'] = $convites['id'];
        $dados_tabela[$key]['nome_afiliado'] = $convites['nome_afiliado'];
        $dados_tabela[$key]['email_afiliado'] = $convites['email_afiliado'];
        $dados_tabela[$key]['termos'] = '<i class="nav-icon fas fa-file"></i>';
        $dados_tabela[$key]['texto_termo'] = $convites['termos'];
        $dados_tabela[$key]['remover'] = '-';
        $dados_tabela[$key]['aceito'] = $convites['aceito'];
        $data_envio = strtotime($convites['data_envio']);

        if ($dados_loja['idioma'] == "ptb") {
          $dados_tabela[$key]['data'] = date('d/m/Y H:i:s', $data_envio);
        } else {
          $dados_tabela[$key]['data'] = date('m/d/Y H:i:s', $data_envio);
        }


        switch ($convites['aceito']) {
          case 0:
            $dados_tabela[$key]['status'] = $this->lang->line("pendente");
            $dados_tabela[$key]['remover'] = '<i class="nav-icon fas fa-trash"></i>';
            break;
          case 1:
            $dados_tabela[$key]['status'] = $this->lang->line("aceito");
            $dados_tabela[$key]['remover'] = '<i class="nav-icon fas fa-trash"></i>';
            break;
          case -1:
            $dados_tabela[$key]['status'] = $this->lang->line("recusado");
            break;
          case -2:
            $dados_tabela[$key]['status'] = $this->lang->line("removido");
            break;
          case -3:
            $dados_tabela[$key]['status'] = $this->lang->line("cancelado");
            break;
        }
      }
    }


    ajax(true, "", $dados_tabela);
  }

  public function cancelarConvite()
  {
    $this->load->model("Afiliados_model", "", true);
    $this->load->model("Usuarios_model", "", true);
    $this->load->library("sendemail");

    $id_afiliado = $this->input->get_post("id_afiliado");

    $convite = $this->Afiliados_model->getConvite($id_afiliado);
    $afiliado = $this->Usuarios_model->getUserByEmail($convite['email_afiliado']);
    if ($convite['aceito'] == 1) {
      $this->lang->load('app_lang', $afiliado['idioma']);
      //Construindo o email
      $data['email_preview_text']  = $this->lang->line("previewConviteSuspenso");
      $assunto  = $this->lang->line("assuntoConviteSuspenso");
      $assunto = str_ireplace("{nome_loja}", $convite['nome_loja'], $assunto);

      $data['email_msg']  = $this->lang->line("msgConviteAfiliadoSuspenso");
      $data['email_msg'] = str_ireplace("{nome_afiliado}", $convite['nome_afiliado'], $data['email_msg']);
      $data['email_msg'] = str_ireplace("{nome_loja}", $convite['nome_loja'], $data['email_msg']);
      $data['link_convite'] = "#";
      $data['convite'] = $this->lang->line("msgConviteAfiliadoSuspensoBotao");
      $data['email_msg_bottom'] = $this->lang->line("msgConviteAfiliadoSuspensoFooter");

      $corpo_email = $this->load->view("emails/convite_afiliado", $data, true);

      $this->sendemail->enviar($convite['email_afiliado'], $convite['nome_afiliado'], $assunto, $corpo_email);
      $resposta = $this->Afiliados_model->suspenderConvite($id_afiliado);

      $this->lang->load('app_lang', $_SESSION['idioma']);
    } else {
      $resposta = $this->Afiliados_model->deletarConvite($id_afiliado);
    }


    if ($resposta) {
      if ($convite['aceito'] == 1) {
        ajax(true, $this->lang->line('conviteSuspensoComSucesso'), "");
      } else {
        ajax(true, $this->lang->line('conviteDeletadoComSucesso'), "");
      }
    } else {
      ajax(false, $this->lang->line('erroGeral'), "");
    }
  }
}
