<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Hashids\Hashids;

class Webhooks extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function ordem_criada()
    {
        $hmac = $this->input->get_request_header('X-Shopify-Hmac-SHA256', TRUE);
        $shared_secret = $this->config->item('shopify_shared_secret');
        $data = $this->input->raw_input_stream;
        if ($this->hashHMAC($hmac, $data, $shared_secret) == false) {
            die;
        }
        $this->load->model("Fila_model", "", true);
        $this->load->model("Lojas_model", "", true);
        $this->load->model("Vendas_model", "", true);
        $this->load->library("Shopify");

        $hashids = new Hashids($this->config->item("id_salt"));

        $order = json_decode($data, true);
        $venda = $this->Vendas_model->getVendaByOrderId($order['id']);

        if ($venda == false) {
            $slfg = false;

            foreach ($order['note_attributes'] as $attributes) {
                if ($attributes['name'] == "__slgf") {
                    $slfg = $attributes['value'];
                }
            }

            if ($slfg == false) {

                $referring_site = $order['referring_site'];
                $parts = parse_url($referring_site);


                parse_str($parts['query'], $query);

                if (!isset($query['slgf'])) {
                    die;
                }

                $slfg = $query['slgf'];
            }

            $ids = $hashids->decode($slfg);

            if (empty($ids)) {
                die;
            }

            $id_afiliado = $ids[0]; // AKA id convite
            $id_usuario = $ids[1];
            $id_loja = $ids[2];

            $dados_loja = $this->Lojas_model->get_loja_shopify_by_id($id_loja);

            if ($dados_loja == false) {
                die;
            }

            $update_order = array(
                "order" => array(
                    "id" => $order['id'],
                    "note_attributes" => array(
                        array(
                            "name" => "Slingfy",
                            "value" => $this->lang->line('processando')
                        )
                    )
                )
            );
    
            $this->shopify->update_order($dados_loja['dominio_shopify'], $dados_loja['access_token'], $update_order, $order['id']);

            $valor = (int) $order['total_price'] * 100;
            $this->Vendas_model->inserirVenda($id_loja, $id_usuario, $id_afiliado, $order['id'], $order['financial_status'], $valor);
        }
        
        $this->Fila_model->inserirOrder($data);
    }

    private  function hashHMAC($hmac, $data, $shared_secret)
    {
        $hmac = bin2hex(base64_decode($hmac));
        $computed_hmac = hash_hmac('sha256', $data, $shared_secret);

        return hash_equals($hmac, $computed_hmac);
    }

    public function remove_store()
    {
        $this->load->model("Lojas_model", "", true);
        $hmac = $this->input->get_request_header('X-Shopify-Hmac-SHA256', TRUE);
        $shared_secret = $this->config->item('shopify_shared_secret');
        $data = $this->input->raw_input_stream;
        if ($this->hashHMAC($hmac, $data, $shared_secret) == false) {
            die;
        }

        $remove_store = $this->input->raw_input_stream;
        $remove_store = json_decode($remove_store, true);

        $loja = $this->Lojas_model->get_loja_shopify_by_name($remove_store['myshopify_domain']);

        $this->Lojas_model->deletarLoja($loja['id']);
    }

    public function costumer()
    {
        $this->load->model("Lojas_model", "", true);
        $hmac = $this->input->get_request_header('X-Shopify-Hmac-SHA256', TRUE);
        $shared_secret = $this->config->item('shopify_shared_secret');
        $data = $this->input->raw_input_stream;
        if ($this->hashHMAC($hmac, $data, $shared_secret) == false) {
            die;
        }

        $array['response'] = "There are not costume data linked to this e-mail on our servers";
        echo json_encode($array);
    }


    public function redact_store()
    {
        $this->load->model("Lojas_model", "", true);
        $hmac = $this->input->get_request_header('X-Shopify-Hmac-SHA256', TRUE);
        $shared_secret = $this->config->item('shopify_shared_secret');
        $data = $this->input->raw_input_stream;
        if ($this->hashHMAC($hmac, $data, $shared_secret) == false) {
            die;
        }

        $remove_store = $this->input->raw_input_stream;
        $remove_store = json_decode($remove_store, true);

        $loja = $this->Lojas_model->get_loja_shopify_by_name($remove_store['shop_domain']);

        $this->Lojas_model->deletarLoja($loja['id']);

        $array['response'] = "Ok";
        echo json_encode($array);
    }
}
