<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Installer extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   *	- or -
   * 		http://example.com/index.php/welcome/index
   *	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */
  public function index()
  {
    $this->load->view("lojas/install");
  }

  public function redirect($shop = null)
  {
    // Set variables for our request
    if ($shop == null) {
      $shop = $this->input->get('shop');
    }
    $shop = str_ireplace(".myshopify.com", "", strtolower($shop));
    $shop = str_ireplace("https://", "", $shop);
    $api_key = $this->config->item('shopify_api_key');
    $scopes = "read_products,read_orders,write_orders,read_script_tags,write_script_tags";
    $redirect_uri = $this->config->item('base_url') . "lojistas/installer/generate_token";


    // Build install/approval URL to redirect to
    $install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

    // Redirect
    header("Location: " . $install_url);
    die();
  }

  public function generate_token()
  {

    $this->load->helper("url");
    $this->load->model("Lojas_model", "", true);
    $this->load->library('shopify');

    // Set variables for our request
    $api_key = $this->config->item('shopify_api_key');
    $shared_secret =  $this->config->item('shopify_shared_secret');
    $params = $this->input->get(); // Retrieve all request parameters

    // Set variables for our request
    $query = array(
      "client_id" => $api_key, // Your API key
      "client_secret" => $shared_secret, // Your app credentials (secret key)
      "code" => $params['code'] // Grab the access key from the URL
    );

    // Generate access token URL
    $access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";


    // Configure curl client and execute request
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $access_token_url);
    curl_setopt($ch, CURLOPT_POST, count($query));
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
    $result = curl_exec($ch);
    curl_close($ch);

    // Store the access token
    $result = json_decode($result, true);
    $access_token = $result['access_token'];

    $loja_ja_existe =  $this->Lojas_model->get_loja_shopify_by_name($params['shop']);

    if ($loja_ja_existe == false) {
      $id_loja = $this->Lojas_model->instalarLoja($params['shop'], $access_token);
    } else {
      $update['access_token'] =  $access_token;
      $id_loja = $loja_ja_existe['id'];
    }


    $resultado = $this->shopify->get_shop_info($params['shop'], $access_token);
    $resultado = json_decode($resultado, true);

    $update["email_contato"] = empty($resultado['shop']['customer_email']) ? $resultado['shop']['email'] : $resultado['shop']['customer_email'];
    $update["nome"] = $resultado['shop']['name'];
    $update["dominio_proprio"] = $resultado['shop']['domain'];
    $update["idioma"] = "en";
    $update["reinstall"] = 0;

    $this->Lojas_model->update_loja($id_loja, $update);

    $this->shopify->turn_on_checkout($params['shop'], $access_token);
    $this->shopify->create_webhooks_order_create($params['shop'], $access_token);
    $this->shopify->create_webhooks_order_paid($params['shop'], $access_token);
    $this->shopify->create_webhooks_uninstall($params['shop'], $access_token);

    
    redirect("https://".$params['shop'].$this->config->item('app_page'));
  }

  public function teste()
  {

    $this->load->library('shopify');
    $resultado = $this->shopify->get_shop_info("devmcp.myshopify.com", "d57ad0e1964240a71c9ab94628caa671");
    var_dump($resultado);
  }
}
