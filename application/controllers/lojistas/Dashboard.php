<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Hashids\Hashids;

class Dashboard extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   *	- or -
   * 		http://example.com/index.php/welcome/index
   *	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */
  public function index()
  {
    $this->load->model("Afiliados_model", "", true);
    $this->load->model("Lojas_model", "", true);
    $this->load->helper('url');
    $this->load->model("Pagamentos_model", "", true);
    $this->load->library("shopify");

    $dominio_shopify = $this->input->get('shop');
    $charge_id = $this->input->get("charge_id");

    $loja = $this->Lojas_model->get_loja_shopify_by_name($dominio_shopify);

    if ($loja == false || $loja['reinstall'] == 1) { 
      redirect('lojistas/installer/redirect/' . $dominio_shopify, "refresh");
    } else if($loja['reinstall'] == 1){
      echo anchor('lojistas/installer/redirect/' . $dominio_shopify, 'This app need to be instaled/reinstaled', array('target' => '_blank'));
      die;
    }else if($_SERVER['HTTP_SEC_FETCH_DEST'] != "iframe"){
      redirect("https://".$dominio_shopify.$this->config->item('app_page'));
    }

    $recorrencia_atual = $this->Pagamentos_model->getLastRecorrenciaByIdLoja($loja['id']);

    $data['recorrencia']['status'] = $recorrencia_atual['status'];

    if ($charge_id) {
      if ($recorrencia_atual != false) {
        $resultado = $this->shopify->get_recurring_charge($dominio_shopify, $loja['access_token'], $recorrencia_atual["id_recorrencia_shopify"]);
        $parsed = json_decode($resultado, true);
        if ($parsed['recurring_application_charge']['status'] == "accepted") {
          $this->shopify->activate_recurring_charge($loja['dominio_shopify'], $loja['access_token'], $recorrencia_atual['id_recorrencia_shopify']);
        }
      }
    }




    $convites = $this->Afiliados_model->getTodosConvitesLojista($loja['id']);

    srand($this->make_seed());

    $hashids = new Hashids($this->config->item("loja_salt"));
    $safeHash = $hashids->encode($loja['id'], time() + rand(60 * 25, 60 * 35), rand(0, getrandmax()));

    $data['convites'] = $convites;
    $data['safe_hash'] = $safeHash;

    $this->load->view("lojas/dashboard", $data);
  }

  private function make_seed()
  {
    list($usec, $sec) = explode(' ', microtime());
    return $sec + $usec * 1000000;
  }

  public function ordersXLSX()
  {
    $id_convite = $this->input->post("id_convite", true);
    $safe_hash = $this->input->post("safe_hash", true);

    $hashids = new Hashids($this->config->item("loja_salt"));
    $ids = $hashids->decode($safe_hash);

    if (empty($ids)) {
      die("?");
    }

    $id_loja = $ids[0];

    if ($ids[1] < time()) {
      die("Expired");
    }

    $this->load->model("Estatisticas_model", "", true);
    $this->load->model("Lojas_model", "", true);


    if ($id_convite == 0) {
      $resposta = $this->Estatisticas_model->getAllVendasByLoja($id_loja);
    } else {
      $resposta = $this->Estatisticas_model->getAllVendasByLojaEConvite($id_loja, $id_convite);
    }

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', $this->lang->line("todasVendasRelatorio"));
    $sheet->setCellValue('A2', $this->lang->line("correcaoGMT"));
    $sheet->setCellValue('B2', $_SESSION['idioma'] == "ptb" ? "-3" : "0");

    $start_line = 3;
    $sheet->setCellValue('A' . $start_line, $this->lang->line("idConvite"));
    $sheet->setCellValue('B' . $start_line, $this->lang->line("orderId"));
    $sheet->setCellValue('C' . $start_line, $this->lang->line("valorTotal"));
    $sheet->setCellValue('D' . $start_line, $this->lang->line("statusPagamento"));
    $sheet->setCellValue('E' . $start_line, $this->lang->line("convite"));
    $sheet->setCellValue('F' . $start_line, $this->lang->line("dataCriada"));
    $sheet->setCellValue('G' . $start_line, $this->lang->line("dataCriadaGMT"));
    $sheet->setCellValue('H' . $start_line, $this->lang->line("dataAtualizada"));
    $sheet->setCellValue('I' . $start_line, $this->lang->line("dataAtualizadaGMT"));

    if ($resposta)
      foreach ($resposta as $key => $linha) {
        $celNumber = ($key + $start_line + 1);
        $sheet->setCellValue('A' . $celNumber, $linha['id_afiliado']);
        $sheet->setCellValue('B' . $celNumber, $linha['order_id']);
        $sheet->setCellValue('C' . $celNumber, $linha['valor'] / 100);
        $sheet->setCellValue('D' . $celNumber, $linha['pago']);
        $sheet->setCellValue('E' . $celNumber, $linha['nome_afiliado']);

        $excelDateValue = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($linha['dt_created_unix']);

        $sheet->setCellValue('F' . $celNumber, $excelDateValue);
        $spreadsheet->getActiveSheet()->getStyle('F' . $celNumber)
          ->getNumberFormat()
          ->setFormatCode(
            \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME
          );
        $sheet->setCellValue('G' . $celNumber, "=F$celNumber+(B2/24)");
        $spreadsheet->getActiveSheet()->getStyle('G' . $celNumber)
          ->getNumberFormat()
          ->setFormatCode(
            \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME
          );

        $excelDateValue = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($linha['dt_updated_unix']);

        $sheet->setCellValue('H' . $celNumber, $excelDateValue);
        $spreadsheet->getActiveSheet()->getStyle('H' . $celNumber)
          ->getNumberFormat()
          ->setFormatCode(
            \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME
          );
        $sheet->setCellValue('I' . $celNumber, "=H$celNumber+(B2/24)");
        $spreadsheet->getActiveSheet()->getStyle('I' . $celNumber)
          ->getNumberFormat()
          ->setFormatCode(
            \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME
          );
      }
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="orders.xlsx"');
    header('Cache-Control: max-age=0');
    $writer = new Xlsx($spreadsheet);
    $writer->save('php://output');
  }

  public function visitasXLSX()
  {
    $id_convite = $this->input->post("id_convite", true);
    $safe_hash = $this->input->post("safe_hash", true);

    $hashids = new Hashids($this->config->item("loja_salt"));
    $ids = $hashids->decode($safe_hash);

    if (empty($ids)) {
      die("?");
    }

    $id_loja = $ids[0];

    if ($ids[1] < time()) {
      die("Expired");
    }

    $this->load->model("Estatisticas_model", "", true);
    $this->load->model("Lojas_model", "", true);

    if ($id_convite == 0) {
      $resposta = $this->Estatisticas_model->getAllVisitasByLoja($id_loja);
    } else {
      $resposta = $this->Estatisticas_model->getAllVisitasByLojaEConvite($id_loja, $id_convite);
    }

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', $this->lang->line("todasVisitasRelatorio"));
    $sheet->setCellValue('A2', $this->lang->line("correcaoGMT"));
    $sheet->setCellValue('B2', $_SESSION['idioma'] == "ptb" ? "-3" : "0");

    $start_line = 3;
    $sheet->setCellValue('A' . $start_line, "id");
    $sheet->setCellValue('B' . $start_line, $this->lang->line("convite"));
    $sheet->setCellValue('C' . $start_line, $this->lang->line("tipoVisita"));
    $sheet->setCellValue('D' . $start_line, $this->lang->line("dataCriada"));
    $sheet->setCellValue('E' . $start_line, $this->lang->line("dataCriadaGMT"));

    if ($resposta)
      foreach ($resposta as $key => $linha) {
        $celNumber = ($key + $start_line + 1);
        $sheet->setCellValue('A' . $celNumber, $linha['id']);
        $sheet->setCellValue('B' . $celNumber, $linha['nome_afiliado']);

        $visita = $linha['primeira'] == 1 ? $this->lang->line('unica') : $this->lang->line('retorno');

        $sheet->setCellValue('C' . $celNumber, $visita);

        $excelDateValue = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($linha['dt_created_unix']);

        $sheet->setCellValue('D' . $celNumber, $excelDateValue);
        $spreadsheet->getActiveSheet()->getStyle('D' . $celNumber)
          ->getNumberFormat()
          ->setFormatCode(
            \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME
          );
        $sheet->setCellValue('E' . $celNumber, "=D$celNumber+(B2/24)");
        $spreadsheet->getActiveSheet()->getStyle('E' . $celNumber)
          ->getNumberFormat()
          ->setFormatCode(
            \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME
          );
      }
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="visits.xlsx"');
    header('Cache-Control: max-age=0');
    $writer = new Xlsx($spreadsheet);
    $writer->save('php://output');
  }
}
