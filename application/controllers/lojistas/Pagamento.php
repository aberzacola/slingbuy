<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pagamento extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   *	- or -
   * 		http://example.com/index.php/welcome/index
   *	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */
  public function index()
  {
    $this->load->model("Afiliados_model", "", true);
    $this->load->model("Lojas_model", "", true);
    $this->load->model("Pagamentos_model", "", true);
    $this->load->library("shopify");

    $charge_id = $this->input->get("charge_id");
    $dominio_shopify = $this->input->get('shop');

    $loja = $this->Lojas_model->get_loja_shopify_by_name($dominio_shopify);

    $recorrencia_atual = $this->Pagamentos_model->getLastRecorrenciaByIdLoja($loja['id']);

    if ($recorrencia_atual != false) {
      $resultado = $this->shopify->get_recurring_charge($dominio_shopify, $loja['access_token'], $recorrencia_atual["id_recorrencia_shopify"]);
      $parsed = json_decode($resultado, true);
      if ($parsed['recurring_application_charge']['status'] == "accepted") {
        $this->shopify->activate_recurring_charge($loja['dominio_shopify'], $loja['access_token'], $recorrencia_atual['id_recorrencia_shopify']);
      }
    }



    $data = array();


    if ($recorrencia_atual != false) {
      $resultado = $this->shopify->get_recurring_charge($dominio_shopify, $loja['access_token'], $recorrencia_atual["id_recorrencia_shopify"]);
      $parsed = json_decode($resultado, true);

      $recorrencia['dias_de_teste'] = $parsed['recurring_application_charge']['trial_days'];
      $recorrencia['dias_de_teste_fim'] = strtotime($parsed['recurring_application_charge']['trial_ends_on']);
      $recorrencia['status'] = $parsed['recurring_application_charge']['status'];
      $recorrencia['limite'] = floatval($parsed['recurring_application_charge']['capped_amount']) * 100;
      $recorrencia['limite_utilizado'] = $parsed['recurring_application_charge']['balance_used'] * 100;
      $recorrencia['limite_restante'] = $parsed['recurring_application_charge']['balance_remaining'] * 100;

      $data['recorrencia']['status'] = $parsed['recurring_application_charge']['status'];
      $data['recorrencia']['id'] = $parsed['recurring_application_charge']['id'];
      $data['recorrencia']['limite'] = $parsed['recurring_application_charge']['capped_amount'];
      $data['recorrencia']['utilizado'] = $parsed['recurring_application_charge']['balance_used'];

      if ($parsed['recurring_application_charge']['status'] == "pending") {
        $data['recorrencia']['url'] = $parsed['recurring_application_charge']['confirmation_url'];
      }

      $this->Pagamentos_model->updateRecorrencia($recorrencia_atual['id'], $recorrencia);
    }

    $this->load->view("lojas/pagamentos", $data);
  }

  public function criarRecorrencia()
  {
    $this->load->library("shopify");
    $this->load->model("Lojas_model", "", true);
    $this->load->model("Pagamentos_model", "", true);

    $limite = $this->input->post("limite");

    $shop = $this->input->get("shop");

    $shop_info = $this->Lojas_model->get_loja_shopify_by_name($shop);

    $existe_recorrencia = $this->Pagamentos_model->getLastRecorrenciaByIdLoja($shop_info['id']);
    $status_permitidos = array("declined", "expired", "cancelled");

    if ($existe_recorrencia == false || in_array($existe_recorrencia['status'], $status_permitidos)) {

      $comando['recurring_application_charge']['name'] = "Slingfy";
      $comando['recurring_application_charge']['price'] = 0;
      $comando['recurring_application_charge']['trial_days'] = 0;
      
      $comando['recurring_application_charge']['return_url'] = "https://".$shop.$this->config->item('app_page');
      $comando['recurring_application_charge']['capped_amount'] = $limite / 100;
      $comando['recurring_application_charge']['terms'] = $this->lang->line("termos_billing");
      $comando['recurring_application_charge']['test'] = $this->config->item('test_mode');

      $dados = $this->shopify->create_recurring_charge($shop, $shop_info['access_token'], $comando);

      $parsed = json_decode($dados, true);

      $recorrencia['id_loja'] = $shop_info['id'];
      $recorrencia['id_recorrencia_shopify'] = $parsed['recurring_application_charge']['id'];
      $recorrencia['dias_de_teste'] = $parsed['recurring_application_charge']['trial_days'];
      $recorrencia['dias_de_teste_fim'] = strtotime($parsed['recurring_application_charge']['trial_ends_on']);
      $recorrencia['status'] = $parsed['recurring_application_charge']['status'];
      $recorrencia['limite'] = floatval($parsed['recurring_application_charge']['capped_amount']) * 100;
      $recorrencia['limite_utilizado'] = $parsed['recurring_application_charge']['balance_used'] * 100;
      $recorrencia['limite_restante'] = $parsed['recurring_application_charge']['balance_remaining'] * 100;

      $this->Pagamentos_model->inserirRecorrencia($recorrencia);

      ajax(true, "Ok", array("confirmation_url" => $parsed['recurring_application_charge']['confirmation_url']));
    } else {

      switch ($existe_recorrencia['status']) {
        case "pending":
          $msg = $this->lang->line("voceTemUmLimitePendente");
          ajax(false, $msg, "");
          break;
        default:
          $msg = $this->lang->line('algoPendente');
          ajax(false, $msg, "");
          break;
      }
    }
  }

  public function atualizaLimite()
  {
    $this->load->library("shopify");
    $this->load->model("Lojas_model", "", true);
    $this->load->model("Pagamentos_model", "", true);

    $limite = $this->input->post("limite");

    $shop = $this->input->get("shop");

    $shop_info = $this->Lojas_model->get_loja_shopify_by_name($shop);

    $existe_recorrencia = $this->Pagamentos_model->getLastRecorrenciaByIdLoja($shop_info['id']);

    $status_permitidos = array("declined", "expired", "cancelled");

    if ($existe_recorrencia['status'] == "active") {
      $retorno = $this->shopify->update_recurring_charge_limit($shop, $shop_info['access_token'], $existe_recorrencia['id_recorrencia_shopify'], $limite / 100);
      $parsed = json_decode($retorno, true);
      if (isset($parsed['errors'])) {
        ajax(false, current($parsed['errors']), "");
      } else {
        ajax(true, "Ok", array("confirmation_url" => $parsed['recurring_application_charge']['update_capped_amount_url']));
      }
    } else {
      $msg = $this->lang->line('naoExisteRecorrenciaAtiva');
      ajax(false, $msg, "");
    }
  }
}
