<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Produtos extends CI_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   *	- or -
   * 		http://example.com/index.php/welcome/index
   *	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */
  public function index()
  {
    $this->load->library("shopify");
    $this->load->model("Lojas_model", "", true);
    $dominio_shopify = $this->input->get_post("shop");
    $dados_loja = $this->Lojas_model->get_loja_shopify_by_name($dominio_shopify);

    $produtos_disponiveis = $this->shopify->get_all_active_products($dominio_shopify, $dados_loja['access_token']);

    // echo $produtos_disponiveis;

    $produtos_disponiveis = json_decode($produtos_disponiveis, true);

    foreach ($produtos_disponiveis['products'] as $i => $produto) {
      $soma = 0;
      foreach ($produto["variants"] as $j => $variants) {
        $soma += $variants['price'];
      }
      $produtos_disponiveis['products'][$i]['soma_variants'] = 'R$ ' . number_format($soma / ($j + 1), 2, ',', '.');
      $produtos_disponiveis['products'][$i]['soma_variants_cents'] = ($soma / ($j + 1)) * 100;
    }

    $data['produtos'] = $produtos_disponiveis['products'];
    $data['porcetagem_comissao'] = $dados_loja['porcetagem_comissao'];

    $this->load->view("lojas/produtos", $data);
  }
}
