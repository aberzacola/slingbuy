<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['post_controller_constructor'][] = array(
    'class'    => '',
    'function' => 'setLang',
    'filename' => 'Lang.php',
    'filepath' => 'hooks',
    'params' => ''
);

$hook['post_controller_constructor'][] = array(
    'class'    => '',
    'function' => 'isLogged',
    'filename' => 'Seguranca.php',
    'filepath' => 'hooks',
    'params' => array(  // controle e metodo de bypass
        "afiliado" => array(
            "cadastro" => array(
                "index" => true,
                "cadastra" => true,
                "login" => true,
                "efetivaLogin" => true,
                'validaEmail' => true,
                'esqueci_senha' => true,
                'recuperar_senha' => true,
                'redefinir_senha' => true,
            )
        ),
        "lojista" => array(
            "geral" => array(
                "mudarIdioma" => true,
                "contadorVisitas" => true,
                "teste" => true,
                "updateWebHooksAndScriptTags" => true
            ),
            "installer" => array(
                "index" => true,
                "redirect" => true,
                "generate_token" => true,
                'checa_loja' => true
            ),
            "dashboard" => array(
                "visitasXLSX" => true,
                "ordersXLSX" => true
            ),
            "webhooks" => array(
                "ordem_criada" => true,
                "remove_store" => true,
                "request_data" => true,
                "redact_store" => true,
                "costumer" => true
            ),
            "afiliados" => array(
                "getTabelaAfiliados" => true,
                "cancelarConvite" => true,
                "convidaAfiliado" => true
            ),
            "crons"=> array(
                "processar_fila_orders" => true
            )
        )
    )
);
