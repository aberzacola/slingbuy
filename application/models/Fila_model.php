<?php

class Fila_model extends CI_Model
{

    public function inserirOrder($order)
    {
        $payload = array(
            "order" => $order
        );

        $this->db->insert('fila_orders', $payload);
        return $this->db->insert_id();
    }

    public function getAllOrders()
    {
        $resultado = $this->db->get("fila_orders");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }

    public function deleteOrder($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('fila_orders');
    }
}
