<?php

class Estatisticas_model extends CI_Model
{

    public function getAllVendasByUsuario($id_usuario)
    {
        $resultado = $this->db->query(
            'SELECT 
        vendas.*, 
        lojas.nome as nome_loja, 
        UNIX_TIMESTAMP(vendas.dt_created) AS dt_created_unix,
        UNIX_TIMESTAMP(vendas.dt_updated) AS dt_updated_unix
        FROM vendas
        LEFT JOIN lojas 
        ON lojas.id = vendas.id_loja
        WHERE id_usuario = ' . $id_usuario
        );

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }

    public function getAllVendasByUsuarioEConvite($id_usuario, $id_convite)
    {
        $resultado = $this->db->query(
            'SELECT 
        vendas.*, 
        lojas.nome as nome_loja, 
        UNIX_TIMESTAMP(vendas.dt_created) AS dt_created_unix,
        UNIX_TIMESTAMP(vendas.dt_updated) AS dt_updated_unix
        FROM vendas
        LEFT JOIN lojas 
        ON lojas.id = vendas.id_loja
        WHERE id_usuario = ' . $id_usuario  . ' AND id_afiliado = ' . $id_convite
        );

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }

    public function getAllVendasByLoja($id_loja)
    {
        $resultado = $this->db->query(
            'SELECT 
        vendas.*, 
        afiliados.nome_afiliado, 
        UNIX_TIMESTAMP(vendas.dt_created) AS dt_created_unix,
        UNIX_TIMESTAMP(vendas.dt_updated) AS dt_updated_unix
        FROM vendas
        LEFT JOIN afiliados 
        ON afiliados.id = vendas.id_afiliado
        WHERE vendas.id_loja = ' . $id_loja
        );

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }

    public function getAllVendasByLojaEConvite($id_loja, $id_convite)
    {
        $resultado = $this->db->query(
            'SELECT 
        vendas.*, 
        afiliados.nome_afiliado, 
        UNIX_TIMESTAMP(vendas.dt_created) AS dt_created_unix,
        UNIX_TIMESTAMP(vendas.dt_updated) AS dt_updated_unix
        FROM vendas
        LEFT JOIN afiliados 
        ON afiliados.id = vendas.id_afiliado
        WHERE vendas.id_loja = ' . $id_loja . ' AND vendas.id_afiliado = ' . $id_convite
        );

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }

    public function getAllVendasByUsuarioUltimos6Meses($id_usuario)
    {
        $resultado = $this->db->query('SELECT vendas.*, lojas.nome as nome_loja
        FROM vendas
        LEFT JOIN lojas 
        ON lojas.id = vendas.id_loja
        WHERE id_usuario = ' . $id_usuario . ' AND vendas.dt_created > DATE_SUB(now(), INTERVAL 6 MONTH)');

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }

    public function getAllVisitasByUsuario($id_usuario)
    {

        $resultado = $this->db->query("SELECT 
        visitas.id, 
        visitas.primeira,
        lojas.nome,
        UNIX_TIMESTAMP(visitas.date_time) AS dt_created_unix
        FROM visitas 
        LEFT JOIN lojas
        ON lojas.id = visitas.id_loja
        WHERE visitas.id_usuario = $id_usuario");

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }

    public function getAllVisitasByUsuarioEConvite($id_usuario, $convite)
    {

        $resultado = $this->db->query("SELECT 
        visitas.id, 
        visitas.primeira,
        lojas.nome,
        UNIX_TIMESTAMP(visitas.date_time) AS dt_created_unix
        FROM visitas 
        LEFT JOIN lojas
        ON lojas.id = visitas.id_loja
        WHERE visitas.id_usuario = $id_usuario
        AND visitas.id_afiliado = $convite");

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }

    public function getAllVisitasByLoja($id_loja)
    {

        $resultado = $this->db->query("SELECT 
        visitas.id, 
        visitas.primeira,
        afiliados.nome_afiliado,
        UNIX_TIMESTAMP(visitas.date_time) AS dt_created_unix
        FROM visitas 
        LEFT JOIN afiliados
        ON afiliados.id = visitas.id_afiliado
        WHERE visitas.id_loja = $id_loja");

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }

    public function getAllVisitasByLojaEConvite($id_loja, $convite)
    {

        $resultado = $this->db->query("SELECT 
        visitas.id, 
        visitas.primeira,
        afiliados.nome_afiliado,
        UNIX_TIMESTAMP(visitas.date_time) AS dt_created_unix
        FROM visitas 
        LEFT JOIN afiliados
        ON afiliados.id = visitas.id_afiliado
        WHERE visitas.id_loja = $id_loja
        AND visitas.id_afiliado = $convite");

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }
}
