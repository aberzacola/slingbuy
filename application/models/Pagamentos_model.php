<?php

class Pagamentos_model extends CI_Model
{

    public function inserirRecorrencia($dados)
    {
        $this->db->insert('recorrencia_lojista', $dados);
        return $this->db->insert_id();
    }

    public function getLastRecorrenciaByIdLoja($id_loja)
    {
        $resultado = $this->db->query(
            "SELECT * FROM recorrencia_lojista WHERE id_loja = $id_loja ORDER BY dt_created DESC LIMIT 1"
        );

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }

    public function getRecorrenciaByIdLoja($id_loja)
    {
        $this->db->where("id_loja", $id_loja);
        $resultado = $this->db->get("recorrencia_lojista");

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }

    public function updateRecorrencia($id, $val)
    {
        $this->db->where("id", $id);
        $this->db->update('recorrencia_lojista', $val);
        return $this->db->affected_rows() == 0 ? false : true;
    }
}
