<?php

class Afiliados_model extends CI_Model
{

    public function inserirConvite($id_loja, $nome_afiliado, $email_afiliado, $termos, $aceito)
    {
        $payload = array(
            "id_loja" => $id_loja,
            "nome_afiliado" => $nome_afiliado,
            "email_afiliado" => $email_afiliado,
            "termos" => $termos,
            "aceito" => $aceito,
        );

        $this->db->insert('afiliados', $payload);
        return $this->db->insert_id();
    }

    public function getConvite($id)
    {
        $resultado = $this->db->query('SELECT 
        afiliados.*, 
        lojas.email_contato AS email_lojista,
        lojas.nome AS nome_loja
        FROM afiliados 
        LEFT JOIN lojas
        ON lojas.id = afiliados.id_loja
        WHERE afiliados.id = ' . $this->db->escape($id));

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows[0];
        }
    }

    public function getTodosConvitesLojista($id_loja)
    {
        $this->db->where('id_loja', $id_loja);
        $this->db->order_by('id', 'DESC');

        $resultado = $this->db->get("afiliados");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }

    public function deletarConvite($id_afiliado)
    {
        $data = array(
            "aceito" => -2
        );
        $this->db->where("id", $id_afiliado);
        $this->db->update('afiliados', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function suspenderConvite($id_afiliado)
    {
        $data = array(
            "aceito" => -3
        );
        $this->db->where("id", $id_afiliado);
        $this->db->update('afiliados', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getTodosConvitesPendentesAfiliado($email_afiliado)
    {
        $resultado = $this->db->query('SELECT 
        afiliados.*, 
        lojas.email_contato AS email_lojista,
        lojas.nome AS nome_loja
        FROM afiliados 
        LEFT JOIN lojas
        ON lojas.id = afiliados.id_loja
        WHERE email_afiliado = ' . $this->db->escape($email_afiliado) . 
        ' AND aceito = 0
        ORDER BY afiliados.id DESC');


        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }

    public function getTodosConvitesAfiliado($email_afiliado)
    {
        $resultado = $this->db->query('SELECT 
        afiliados.*, 
        lojas.email_contato AS email_lojista,
        lojas.nome AS nome_loja
        FROM afiliados 
        LEFT JOIN lojas
        ON lojas.id = afiliados.id_loja
        WHERE email_afiliado = ' . $this->db->escape($email_afiliado) .
        ' ORDER BY afiliados.id DESC');




        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }

    public function getTodosConvitesAceitosAfiliado($email_afiliado)
    {
        $resultado = $this->db->query('SELECT 
        afiliados.*, 
        lojas.email_contato AS email_lojista,
        lojas.nome AS nome_loja
        FROM afiliados 
        LEFT JOIN lojas
        ON lojas.id = afiliados.id_loja
        WHERE afiliados.email_afiliado = ' . $this->db->escape($email_afiliado) .
        ' AND afiliados.aceito = 1'.
        ' ORDER BY afiliados.id DESC');




        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }

    public function aceitaConvite($id_convite)
    {

        $data = array(
            "aceito" => 1
        );
        $this->db->where("id", $id_convite);
        $this->db->update('afiliados', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function recusaConvite($id_convite)
    {

        $data = array(
            "aceito" => -1
        );
        $this->db->where("id", $id_convite);
        $this->db->update('afiliados', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
