<?php

class Lojas_model extends CI_Model
{

    public function instalarLoja($shop, $token)
    {
        $payload = array(
            "dominio_shopify" => $shop,
            "access_token" => $token,
        );

        $this->db->insert('lojas', $payload);
        return $this->db->insert_id();
    }

    public function deletarLoja($id_loja)
    {
        $this->db->where("id", $id_loja);
        $this->db->delete('lojas');
    }

    public function update_loja($id_loja, $val)
    {
        $this->db->where("id", $id_loja);
        $this->db->update('lojas', $val);
        return $this->db->affected_rows() == 0 ? false : true;
    }


    public function get_loja_shopify_by_name($shopify_nome)
    {

        $this->db->where('dominio_shopify', $shopify_nome);

        $resultado = $this->db->get("lojas");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }

    public function get_loja_shopify_by_id($id)
    {

        $this->db->where('id', $id);

        $resultado = $this->db->get("lojas");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }
}