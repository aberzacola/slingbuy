<?php

class Usuarios_model extends CI_Model
{

    public function verificaEmail($email)
    {
        $this->db->where("email", $email);
        $this->db->get("usuarios");
        return $this->db->affected_rows() == 0 ? false : true;
    }

    public function guardaCadastro($nome, $email, $senha, $ativo, $idioma)
    {
        $senha = password_hash($senha, PASSWORD_BCRYPT);
        $payload = array(
            "nome" => $nome,
            "email" => $email,
            "senha" => $senha,
            "ativo" => $ativo,
            "idioma" => $idioma
        );

        $this->db->insert('usuarios', $payload);
        return $this->db->insert_id();
    }

    public function getUserByEmail($email)
    {
        $this->db->where("email", $email);
        $resultado = $this->db->get("usuarios");

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }

    public function updateUsuario($id, $val)
    {
        if (isset($val['senha'])) {
            $val['senha'] = password_hash($val['senha'], PASSWORD_BCRYPT);
        };
        $this->db->where("id", $id);
        $this->db->update('usuarios', $val);
        return $this->db->affected_rows() == 0 ? false : true;
    }

    public function getUsuario($id_usuario)
    {
        $this->db->where("id", $id_usuario);
        $resultado = $this->db->get("usuarios");

        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }

    public function getDataUpdate()
    {
    }
}
