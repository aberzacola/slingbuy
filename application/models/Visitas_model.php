<?php

class Visitas_model extends CI_Model
{

    public function inserirVisita($id_loja, $id_usuario, $id_afiliado, $primeira)
    {
        $payload = array(
            "id_loja" => $id_loja,
            "id_usuario" => $id_usuario,
            "id_afiliado" => $id_afiliado,
            "primeira" => $primeira
        );

        $this->db->insert('visitas', $payload);
        return $this->db->insert_id();
    }
}