<?php

class Vendas_model extends CI_Model
{
    public function getVendaByOrderId($order_id)
    {
        $this->db->where('order_id', $order_id);

        $resultado = $this->db->get("vendas");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $row = $resultado->result_array();
            return $row[0];
        }
    }

    public function getAllVendasByUsuario($id_usuario){
        $this->db->where('id_usuario', $id_usuario);

        $resultado = $this->db->get("vendas");
        if ($resultado->num_rows() <= 0) {
            return false;
        } else {
            $rows = $resultado->result_array();
            return $rows;
        }
    }


    public function inserirVenda($id_loja, $id_usuario, $id_afiliado, $order_id, $pago, $valor)
    {
        $payload = array(
            "order_id" => $id_loja,
            "id_loja" => $id_loja,
            "id_usuario" => $id_usuario,
            "id_afiliado" => $id_afiliado,
            "order_id" => $order_id,
            "pago" => $pago,
            "valor" => $valor
        );

        $this->db->insert('vendas', $payload);
        return $this->db->insert_id();
    }

    public function updateStatusPagamento($id_venda,$status_pagamento)
    {
        $val['pago'] = $status_pagamento;

        $this->db->where("id", $id_venda);
        $this->db->update('vendas', $val);
        return $this->db->affected_rows() == 0 ? false : true;
    }

    public function updateCobrancaShopifyFeita($id_venda,$status_pagamento)
    {
        $val['cobrado'] = $status_pagamento;

        $this->db->where("id", $id_venda);
        $this->db->update('vendas', $val);
        return $this->db->affected_rows() == 0 ? false : true;
    }
}
