<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Sendemail
{
    

    public function enviar($email_afiliado, $nome_afiliado, $assunto, $mensagem){

        $CI = &get_instance();

        $mail = new PHPMailer(true);

        try {
            //Server settings                                  // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host       = 'slingfy.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   =  $CI->config->item("hostgator_email_user");                // SMTP username
            $mail->Password   = $CI->config->item("hostgator_email_pass");                               // SMTP password
            $mail->SMTPSecure = 'ssl';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('noreply@slingfy.com' , "Slingfy");       // Name is optional
            $mail->addAddress($email_afiliado , $nome_afiliado);       // Name is optional

            // Content
            $mail->isHTML(true);         // Set email format to HTML
            $mail->CharSet = 'UTF-8';                           
            $mail->Subject = $assunto;
            $mail->Body    = $mensagem;

            $mail->send();
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }

}
