<?php

function ajax($success, $msg, $data)
{
    $output = array("msg" => $msg, "success" => $success, "data" => $data);
    echo json_encode($output);
}
