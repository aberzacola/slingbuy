<?php


function hashSignature($hmac, $params, $shared_secret)
{

    $params = array_diff_key($params, array('signature' => '')); // Remove hmac from params

    ksort($params); // Sort params lexographically
    $computed_hmac = hash_hmac('sha256', urldecode(http_build_query($params, '', '')), $shared_secret);

    return hash_equals($hmac, $computed_hmac);
}

function hashHMAC($hmac, $params, $shared_secret)
{
    $params = array_diff_key($params, array('hmac' => '')); // Remove hmac from params

    ksort($params); // Sort params lexographically
    $computed_hmac = hash_hmac('sha256', http_build_query($params), $shared_secret);

    return hash_equals($hmac, $computed_hmac);
}

function isLogged($bypass)
{
    
    $CI = &get_instance();
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    $bypass = array_change_key_case_recursive($bypass);
    $controller = strtolower($CI->router->fetch_class());
    $method = strtolower($CI->router->fetch_method());
    $shopify = $CI->input->get('shop');
    $modulos = ['afiliado', 'lojista'];
    
    
    foreach ($modulos as $modulo) {
        if (isset($bypass[$modulo][$controller][$method]) && $bypass[$modulo][$controller][$method] === true) {
            return;
        }
    }

    $CI->load->helper('url');
    
    if (!isset($_SESSION['id_usuario'])) {
        if ($shopify == null) {
            redirect('/afiliados/cadastro/login/1');
        }else{
            isFromShopify($bypass);
        }
    }
}

function isFromShopify($bypass)
{
    $CI = &get_instance();
    $isShopify = false;

    $shopify_safe =  $CI->config->item('shopify_safe');
    $controller = $CI->router->fetch_class();
    $method = $CI->router->fetch_method();
    $modulos = ['afiliado', 'lojista'];

    foreach ($modulos as $modulo) {
        if (isset($bypass[$modulo][$controller][$method]) && $bypass[$modulo][$controller][$method] === true) {
            return;
        }
    }

    if ($shopify_safe == false) {
        return;
    }
    
    $params = $CI->input->get(); // Retrieve all request parameters
    $hmac = $CI->input->get('hmac');
    $shared_secret =  $CI->config->item('shopify_shared_secret');

    if ($hmac) {
        $isShopify = hashHMAC($hmac, $params, $shared_secret);
    } else {
        $hmac = $CI->input->get('signature');
        $isShopify = hashSignature($hmac, $params, $shared_secret);
    }

    if (!$isShopify) {
        die("Is not from shopify");
    }
}

function array_change_key_case_recursive($arr)
{
    return array_map(function($item){
        if(is_array($item))
            $item = array_change_key_case_recursive($item);
        return $item;
    },array_change_key_case($arr));
}
