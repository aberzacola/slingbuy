<?php


function setLang()
{
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    $CI = &get_instance();
    $CI->load->model("Lojas_model", "", true);
    $CI->load->model("Usuarios_model", "", true);

    $shop = $CI->input->get_post("shop");

    if (isset($_SESSION['idioma'])) {
        $idioma = $_SESSION['idioma'];
    } else {

        if ($shop != null) {
            $info_loja = $CI->Lojas_model->get_loja_shopify_by_name($shop);
            $idioma = $info_loja['idioma'];
        } elseif (isset($_SESSION['id_usuario'])) {
            $usuario = $CI->Usuarios_model->getUsuario($_SESSION['id_usuario']);
            $idioma = $usuario['idioma'];
        } else {
            $idioma = "en";
        }

        $_SESSION['idioma'] = $idioma;
    }

    if ($shop != null) {
        $info_loja = $CI->Lojas_model->get_loja_shopify_by_name($shop);
        $update['idioma'] = $idioma;
        $CI->Lojas_model->update_loja($info_loja['id'], $update);
    } elseif (isset($_SESSION['id_usuario'])) {
        $update['idioma'] = $idioma;
        $CI->Usuarios_model->updateUsuario($_SESSION['id_usuario'], $update);
    }




    if ($idioma == "ptb") {
        $CI->lang->load('app_lang', 'ptb');
    } else {
        $CI->lang->load('app_lang', 'en');
    }
}
