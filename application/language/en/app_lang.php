<?php

$lang['dashboard'] = 'Dashboard';
$lang['afiliados'] = 'Affiliates';

$lang['erroGeral'] = 'A error occurred, contact support';


//Pag Afiliados
$lang['afiliadoNomeTitulo'] = 'Name';
$lang['afiliadoNomePlaceHolder'] = 'Invite Name';
$lang['convidarAfiliadoTitulo'] = 'Invite Affiliate';
$lang['gestaoDeAfiliadosTitulo'] = 'Affiliates Invite Management';
$lang['afiliadoEmailTitulo'] = 'Email';
$lang['afiliadoEmailPlaceholder'] = 'Affiliate email';
$lang['porcentagemAfiliadoTitulo'] = 'Percentage';
$lang['porcentagemAfiliadoHelp'] = 'Calculated percentage for this affiliate';
$lang['porcentagemAfiliadoPlaceHolder'] = 'Percentage for this affiliate';
$lang['convidarAfiliadoButton'] = 'Invite';
$lang['afiliadoTermos'] = 'Terms:';
$lang['afiliadoTermosExplicacao'] = 'These are the terms that the affiliate must agreed to become an affiliate of your store';

$lang['conviteEnviadoComSucesso'] = "Invite sent successfully";
$lang['termos'] = "Terms";
$lang['pendente'] = "Pending";
$lang['aceito'] = "Accepted";
$lang['recusado'] = "Declined";
$lang['cancelado'] = "Canceled";
$lang['cancelar'] = "Cancel";
$lang['removido'] = "Removed";
$lang['fechar'] = "Close";
$lang['processando'] = "Processing";
$lang['nome'] = "Name";
$lang['data'] = "Date";
$lang['remover'] = "Remove";
$lang['convitesEnviados'] = "Invites Sent";
$lang['convitePendenteErro'] = "Only pending/accepted invites can be canceled";
$lang['conviteDeletadoComSucesso'] = "The invite was canceled successfully";


$lang['previewConviteAfiliado'] = "Slingfy - invite";
$lang['assuntoConviteAfiliado'] = "You received an affiliation invite";
$lang['msgConviteAfiliado'] = "<p>Hello {nome_afiliado}</p><p>You received an affiliation invite from {nome_loja} store</p>";
$lang['msgConviteAfiliadoBotao'] = "See invite";
$lang['msgConviteAfiliadoFooter'] = "You was invited to be a affiliate using Slingfy, congratulations!";

$lang['afiliadLoginComecar'] = "Sign in to continue";
$lang['escolhaSeuIdioma'] = "Choose your language";

$lang['senha'] = "Password";
$lang['entrar'] = "Sign In";
$lang['esqueciMinhaSenha'] = "Forgot my password";
$lang['queroMeCadastrar'] = "Sign up";
$lang['cadastrese'] = "Sign up";
$lang['nomeCompleto'] = "Full name";
$lang['email'] = "Email";
$lang['confirmeASenha'] = "Confirm password";
$lang['cadastro'] = "Sign up";
$lang['euConcordo'] = "I agree";
$lang['cadastrar'] = "Sign up";
$lang['euJaTenhoUmaConta'] = "I already have an account";
$lang['nomeInvalido'] = "Invalid name";
$lang['emailInvalido'] = "Invalid email";
$lang['senhasNaoSaoIguais'] = "The passwords do not match";
$lang['concordeComOsTermos'] = "You must agree with terms";
$lang['emailJaCadastrado'] = "Email already used";
$lang['cadastroEfetuadoComSucessoEmail'] = "Successfully registered, verify your email";

$lang['previewCadastroAfiliaddo'] = "Slingfy - Email verification";
$lang['assuntoCadastroAfiliado'] = "Confirm you email to complete the registration";
$lang['msgCadastroAfiliado'] = "<p>Hello {nome_afiliado}</p><p>To complete your registration click the button bellow</p>";
$lang['msgCadastroConfirmar'] = "Verify email";
$lang['msgCadastroFooter'] = "You will become part of Slingfy community , congratulations!";

$lang['emailVerificado'] = "You registration id confirmed, you will be redirected to the dashboard";
$lang['falhaVerificacaoEmail'] = "Something went wrong, try again";

$lang['convites'] = "Invites";

$lang['errorLogin'] = "Your session expired, sign in";
$lang['logadoComSucesso'] = "Welcome, you will be redirect";
$lang['logadoErro'] = "Email or Password incorrect";

$lang['verificacaoEmailTitle'] = "Email validation";
$lang['recuperacaoSenhaTitle'] = "Password recovery";
$lang['recuperarSenha'] = "Recover password";


$lang['efetuarLogin'] = "Sign In";
$lang['emailNaoEncontrado'] = "Email not found";

$lang['previewRecuperaSenhaAfiliado'] = "Slingfy - Password recovery";
$lang['msgRecuperarSenhaAfiliado'] = "<p>Hello, do you requested password recovery? click bellow to reset your password</p>";
$lang['msgRecuperarSenhaAfiliadoButton'] = "Reset password";
$lang['msgRecuperarSenhaAfiliadoFooter'] = "If you don't requested password recovery, disconsider this email";
$lang['assuntoRecuperarSenhaAfiliado'] = "Password recovery requested";
$lang['emailRecuperacaoSenhaEnviado'] = "A email was sent to you to proceed with the password recovery";
$lang['suaNovaSenhaE'] = "Your new password is: <br />{senha}<br />Remember to change after you login";

$lang['falhaVerificacaoNovaSenha'] = "Something went wrong, try again";

$lang['configuracoesAfiliadoTitulo'] = "Settings";
$lang['configuracoesTrocarSenha'] = "Change password";

$lang['configuracoesSenhaAtual'] = "Current password";
$lang['configuracoesNovaSenha'] = "New password";
$lang['configuracoesNovaSenhaConfirmar'] = "Confirm new password";

$lang['alterarSenha'] = "Update password";

$lang['dicasSenha'] = "Your password should have at least 6 numbers/letters";

$lang['configuracoesAfiliadoSenhaIncorreta'] = "Your current password are incorrect";
$lang['configuracoesAfiliadoSenhaAlteradaSucesso'] = "Your password was successfully updated";

$lang['convitesAfiliadosTitulo'] = "Invites";

$lang['convitesAfiliadosGerenciamentoConvites'] = "Invites management";

$lang['convitesAfiliadosConvitesRecebidos'] = "Received invites";

$lang['loja'] = "Shop";

$lang['aceitar'] = "Accept";
$lang['recusar'] = "Refuse";

$lang['acao'] = "Action";

$lang['convitePendenteErroAfiliado'] = "An action can only be taken on pending/accepted invites";
$lang['conviteAceitoAfiliado'] = "Invite accepted successfully, you can view your divulgation links on 'Shops' section";
$lang['conviteRecusadoAfiliado'] = "Invite successfully refused";


$lang['previewConviteAceito'] = "Slingfy - Your invite was accepted";
$lang['assuntoConviteAceito'] = "Your invite sent to {nome_afiliado} was accepted";
$lang['msgConviteAfiliadoAceito'] = "<p>Hello {nome_lojista}</p><p>Your invite sent to {nome_afiliado} was accepted</p>";
$lang['msgConviteAfiliadoAceitoBotao'] = "Acess Slingfy app to check details";
$lang['msgConviteAfiliadoAceitoFooter'] = "Your invite was accepted, good sales!";

$lang['previewConviteRecusado'] = "Slingfy - Your invite was refused";
$lang['assuntoConviteRecusado'] = "Your invite sent to {nome_afiliado} was refused";
$lang['msgConviteAfiliadoRecusado'] = "<p>Hello {nome_lojista}</p><p>Your invite sent to {nome_afiliado} was refused</p>";
$lang['msgConviteAfiliadoRecusadoBotao'] = "Acess Slingfy app to check details";
$lang['msgConviteAfiliadoRecusadoFooter'] = "You invite was refused, get in touch with the affiliate!";

$lang['previewConviteSuspenso'] = "Slingfy - your affiliation was canceled";
$lang['assuntoConviteSuspenso'] = "Your affiliation with the shop {nome_loja} was canceled";
$lang['msgConviteAfiliadoSuspenso'] = "<p>Hello {nome_afiliado}</p><p>Your affiliation with the shop {nome_loja} was canceled</p>";
$lang['msgConviteAfiliadoSuspensoBotao'] = "Access the website to knwo more";
$lang['msgConviteAfiliadoSuspensoFooter'] = "Your affiliation was canceled, but don't worry, others opportunities will appear :)";

$lang['conviteSuspensoComSucesso'] = "The affiliation was successfully canceled";

$lang['previewConviteSuspensoLojista'] = "Slingfy - One affiliation was canceled";
$lang['assuntoConviteSuspensoLojista'] = "Your affiliation with {nome_afiliado} was canceled";
$lang['msgAfiliacaoSuspensaLojista'] = "<p>Hello {nome_loja}</p><p>Your affiliation with {nome_afiliado} was canceled</p>";
$lang['msgAfiliacaoSuspensaLojistaBotao'] = "Acess Slingfy app to check details";
$lang['msgAfiliacaoSuspensaLojistaFooter'] = "Your affiliation was canceled, but don't worry, others opportunities will appear :)";

$lang['lojas'] = "Shops";

$lang['imagens'] = "Images";
$lang['imagem'] = "Image";
$lang['linkDivulgacao'] = "Affiliate Link";
$lang['produtos'] = "Products";

$lang['filtro'] = "Filter";
$lang['filtroPlaceholder'] = "Search product name";

$lang['geradorDeLink'] = "Affiliate link generator";
$lang['geradorDeLinkPlaceholder'] = "Paste a shop link here to generate the affiliate link";
$lang['gerar'] = "Generate";
$lang['linkGeradoSucesso'] = "Affiliate link generated";
$lang['linkGeradoErro'] = "Invalid URL";
$lang['jaELinkAfiliado'] = "It is already an affiliate link";
$lang['metricasGerais'] = "All metrics";
$lang['todasAsVendas'] = "All sales of the last six months";
$lang['todasAsVisitas'] = "All visits of the last six months";
$lang['valorTotal'] = "Total value";

$lang['unicas'] = "Unique";
$lang['retornos'] = "Returns";

$lang['todosConvites'] = "All Invites";
$lang['periodo'] = "Period";
$lang['downloadEstatisticas'] = "Download Data ( XLS )";
$lang['visitasOuOrdens'] = "Visits or Orders";
$lang['visitas'] = "Visits";
$lang['ordens'] = "Orders";
$lang['selecione'] = "Select...";
$lang['selecioneVisitasOuOrdens'] = "You must choose visits or orders";
$lang['todasVendasRelatorio'] = "All sales";
$lang['idConvite'] = "Invite ID";
$lang['orderId'] = "Order ID";
$lang['valorTotal'] = "Total Order Value";
$lang['statusPagamento'] = "Payment Status";
$lang['nomeLoja'] = "Shop name";
$lang['dataCriada'] = "Creation Date";
$lang['dataCriadaGMT'] = "Creation Date (GMT Adjusted)";
$lang['dataAtualizada'] = "Last update date";
$lang['dataAtualizadaGMT'] = "Last update date (GMT Adjusted)";
$lang['correcaoGMT'] = "Your GMT:";

$lang['todasVisitasRelatorio'] = "All visits";
$lang['tipoVisita'] = "Kind of visit";
$lang['unica'] = "Unique";
$lang['retorno'] = "Returning";


$lang['notasAdicionais'] = "Sell made by: {nomeUsuario}\n Invite id: {id_convite}";
$lang['convite'] = "Invite";

$lang['logout'] = "Log out";
$lang['pagamentos'] = "Billing";
$lang['configPagamentos'] = "Billing Settings";

$lang['limiteDeGasto'] = "Billing Limit ( USD )";
$lang['limiteGastosHelp'] = "Choose the max amount that we are allowed to charge you per month ( in USA dollars )";
$lang['cadastrarLimite'] = "Set limit";
$lang['limiteAtutal'] = "Current limit";
$lang['termos_billing'] = "Will be charged 0.5% of confirmed payed order that is tracked by Slingfy";
$lang['valor_minimo_pagamento'] = "Minimum amount U$ 10.00";

$lang['exportarDados'] = "Export data";
$lang['convitesHelp'] = "If you want to create a billing associated to an invite, choose above";
$lang['alerta'] = "Warning";

$lang['cobrancaPendente'] = "Your limit proposal are waiting for your to accept";
$lang['aprovar'] = "Accept";
$lang['valorCobranca'] = "Amount:";
$lang['status'] = "Status";
$lang['configureSeuPagamento'] = "You should set up your billing settings in order to use Slingfy";
$lang['configureSeuPagamentoTrial'] = "If it is your first time using Slingfy, you have 15 trial days";
$lang['situacaoAtual'] = "Current status";
$lang['voceUtilizou'] = "You used ";
$lang['doSeuLimite'] = " of your limit";

$lang['atualizarLimite'] = "Update limit";
$lang['algoPendente'] = "There is something pending, impossible to take this action now";
$lang['alterarValorLimite'] = "You can change the limit after accepting it, anytime";
$lang['naoExisteRecorrenciaAtiva'] = "Limit change is only possible for active recurrencies";
$lang['novoValorMaiorQueLimite'] = "The new limit amount can't be higher than the used amount";

$lang['certezaQueDesejaCancelar'] = "Your invite will be deleted, are you sure?";

$lang['ajuda'] = "Help";
$lang['jaInstalado'] = "SlingFy is already installed";
$lang['extrato'] = "Tracked Paid Order - Order ID: {orderId} - Invite ID: {conviteId}";
$lang['afiliadoNaoAceitouConvite'] = "Invite not accepted - Slingfy do not charged for this order";


$lang['convitesPendentes'] = "You have pending invites, go to Invites menu to take an action";