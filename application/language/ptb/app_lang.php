<?php

$lang['dashboard'] = 'Dashboard';
$lang['afiliados'] = 'Afiliados';

$lang['erroGeral'] = 'Ocorreu um erro, entre com contato com o suporte';


//Pag Afiliados
$lang['afiliadoNomeTitulo'] = 'Nome';
$lang['afiliadoNomePlaceHolder'] = 'Nome do Convite';
$lang['convidarAfiliadoTitulo'] = 'Convidar Afiliado';
$lang['gestaoDeAfiliadosTitulo'] = 'Gestão de Convites de Afiliados';
$lang['afiliadoEmailTitulo'] = 'Email';
$lang['afiliadoEmailPlaceholder'] = 'Email do afiliado';
$lang['porcentagemAfiliadoTitulo'] = 'Porcentagem';
$lang['porcentagemAfiliadoHelp'] = 'Porcentagem calculada para esse afiliado';
$lang['porcentagemAfiliadoPlaceHolder'] = 'Porcentagem para esse afiliado';
$lang['convidarAfiliadoButton'] = 'Convidar';
$lang['afiliadoTermos'] = 'Termos:';
$lang['afiliadoTermosExplicacao'] = 'Este são os termos que o afiliado tem que concordar antes de se tornar um afiliado de sua loja';

$lang['conviteEnviadoComSucesso'] = "Convite enviado com sucesso";
$lang['termos'] = "Termos";
$lang['pendente'] = "Pendente";
$lang['aceito'] = "Aceito";
$lang['recusado'] = "Recusado";
$lang['cancelado'] = "Cancelado";
$lang['cancelar'] = "Cancelar";
$lang['removido'] = "Removido";
$lang['fechar'] = "Fechar";
$lang['processando'] = "Processando";
$lang['nome'] = "Nome";
$lang['data'] = "Data";
$lang['remover'] = "Remover";
$lang['convitesEnviados'] = "Convites Enviados";
$lang['convitePendenteErro'] = "Somente convites pendentes/aceitos podem ser cancelados";
$lang['conviteDeletadoComSucesso'] = "O convite foi cancelado com sucesso";


$lang['previewConviteAfiliado'] = "Slingfy - convite";
$lang['assuntoConviteAfiliado'] = "Você recebeu um convite de afiliação";
$lang['msgConviteAfiliado'] = "<p>Olá {nome_afiliado}</p><p>Você recebeu um convite da loja {nome_loja}</p>";
$lang['msgConviteAfiliadoBotao'] = "Ver convite";
$lang['msgConviteAfiliadoFooter'] = "Você foi convidado para ser afiliado utilizando o Slingfy, parabéns!";

$lang['afiliadLoginComecar'] = "Identifique-se para iniciar";
$lang['escolhaSeuIdioma'] = "Escolha seu Idioma";

$lang['senha'] = "Senha";
$lang['entrar'] = "Entrar";
$lang['esqueciMinhaSenha'] = "Esqueci minha senha";
$lang['queroMeCadastrar'] = "Quero me cadastrar";
$lang['cadastrese'] = "Cadastre-se";
$lang['nomeCompleto'] = "Nome completo";
$lang['email'] = "Email";
$lang['confirmeASenha'] = "Confirme a senha";
$lang['cadastro'] = "Cadastro";
$lang['euConcordo'] = "Eu concordo";
$lang['cadastrar'] = "Cadastrar";
$lang['euJaTenhoUmaConta'] = "Eu já tenho uma conta";
$lang['nomeInvalido'] = "Nome inválido";
$lang['emailInvalido'] = "Email inválido";
$lang['senhasNaoSaoIguais'] = "As senhas não são iguais";
$lang['concordeComOsTermos'] = "Concorde com os termos";
$lang['emailJaCadastrado'] = "Email já cadastrado";
$lang['cadastroEfetuadoComSucessoEmail'] = "Cadastro efetuado com sucesso, verifique seu email";


$lang['previewCadastroAfiliaddo'] = "Slingfy - Verificação de Email";
$lang['assuntoCadastroAfiliado'] = "Confirme seu email para concluir o cadastro";
$lang['msgCadastroAfiliado'] = "<p>Olá {nome_afiliado}</p><p>Para concluir seu cadastro clique no botão abaixo</p>";
$lang['msgCadastroConfirmar'] = "Verificar email";
$lang['msgCadastroFooter'] = "Você irá fazer parte da comunidade Slingfy, parabéns!";

$lang['emailVerificado'] = "Seu cadastro foi confiramdo com sucesso, vamos te redirecionar para o dashboard";
$lang['falhaVerificacaoEmail'] = "Algo deu errrado, tente novamente";

$lang['convites'] = "Convites";

$lang['errorLogin'] = "Sua sessão expirou, entre novamente";
$lang['logadoComSucesso'] = "Seja bem vindo, você será redirecionado";
$lang['logadoErro'] = "Email ou Senha incorreto";

$lang['verificacaoEmailTitle'] = "Validação de email";
$lang['recuperacaoSenhaTitle'] = "Recuperação de senha";
$lang['recuperarSenha'] = "Recuperar senha";

$lang['efetuarLogin'] = "Efetuar Login";
$lang['emailNaoEncontrado'] = "Email não encontrado";

$lang['previewRecuperaSenhaAfiliado'] = "Slingfy - Recuperação de senha";
$lang['msgRecuperarSenhaAfiliado'] = "<p>Olá, você solicitou a redefinição de senha? clique abaixo para redefinir sua senha</p>";
$lang['msgRecuperarSenhaAfiliadoButton'] = "Redefinir senha";
$lang['msgRecuperarSenhaAfiliadoFooter'] = "Caso você não tenha solicitado a recuperação de senha, desconsidere este email";
$lang['assuntoRecuperarSenhaAfiliado'] = "Recuperação de senha solicitada";
$lang['emailRecuperacaoSenhaEnviado'] = "Um email foi enviado para você proseguir com a recuperação da senha";
$lang['suaNovaSenhaE'] = "Sua nova senha é: <br />{senha}<br />Lembre-se de troca-la assim que efetuar o login";

$lang['falhaVerificacaoNovaSenha'] = "Algo deu errrado, tente novamente";

$lang['configuracoesAfiliadoTitulo'] = "Configurações";
$lang['configuracoesTrocarSenha'] = "Alterar senha";

$lang['configuracoesSenhaAtual'] = "Senha Atual";
$lang['configuracoesNovaSenha'] = "Nova senha";
$lang['configuracoesNovaSenhaConfirmar'] = "Confirmar nova senha";

$lang['alterarSenha'] = "Alterar Senha";

$lang['dicasSenha'] = "Sua senha deve ser composta no mínimo por 6 letras/números";

$lang['configuracoesAfiliadoSenhaIncorreta'] = "A senha atual está incorreta";
$lang['configuracoesAfiliadoSenhaAlteradaSucesso'] = "A sua senha foi alterada com sucesso";


$lang['convitesAfiliadosTitulo'] = "Convites";

$lang['convitesAfiliadosGerenciamentoConvites'] = "Gerenciamento de convites";
$lang['convitesAfiliadosConvitesRecebidos'] = "Convites recebidos";

$lang['loja'] = "Loja";

$lang['aceitar'] = "Aceitar";
$lang['recusar'] = "Recusar";

$lang['acao'] = "Ação";

$lang['convitePendenteErroAfiliado'] = "Somente convites pendentes/aceitos podem sofrer alguma ação";
$lang['conviteAceitoAfiliado'] = "Convite aceito com sucesso, você já pode vizualisar seus links de divulgação na área 'Lojas'";
$lang['conviteRecusadoAfiliado'] = "Convite recusado com sucesso";

$lang['previewConviteAceito'] = "Slingfy - Seu convite foi aceito";
$lang['assuntoConviteAceito'] = "Seu convite enviado para {nome_afiliado} foi aceito";
$lang['msgConviteAfiliadoAceito'] = "<p>Olá {nome_lojista}</p><p>O convite enviado para {nome_afiliado} foi aceito</p>";
$lang['msgConviteAfiliadoAceitoBotao'] = "Acesse o app Slingfy para ver detalhes";
$lang['msgConviteAfiliadoAceitoFooter'] = "Seu convite foi aceito, boas vendas!";


$lang['previewConviteRecusado'] = "Slingfy - Seu convite foi Recusado";
$lang['assuntoConviteRecusado'] = "Seu convite enviado para {nome_afiliado} foi recusado";
$lang['msgConviteAfiliadoRecusado'] = "<p>Olá {nome_lojista}</p><p>O convite enviado para {nome_afiliado} foi recusado</p>";
$lang['msgConviteAfiliadoRecusadoBotao'] = "Acesse o app Slingfy para ver detalhes";
$lang['msgConviteAfiliadoRecusadoFooter'] = "Seu convite foi recusado, entre em contato com o afiliado!";

$lang['previewConviteSuspenso'] = "Slingfy - Sua afiliação foi suspensa";
$lang['assuntoConviteSuspenso'] = "Sua afiliação com a loja {nome_loja} foi suspensa";
$lang['msgConviteAfiliadoSuspenso'] = "<p>Olá {nome_afiliado}</p><p>A sua afiliação com a loja {nome_loja} foi suspensa</p>";
$lang['msgConviteAfiliadoSuspensoBotao'] = "Acesse o site para saber mais";
$lang['msgConviteAfiliadoSuspensoFooter'] = "Sua afiliação foi suspensa, mas não se preocupe, outras oportunidades virão :)";

$lang['conviteSuspensoComSucesso'] = "A afiliação foi suspensa com sucesso";

$lang['previewConviteSuspensoLojista'] = "Slingfy - Uma afiliação foi suspensa";
$lang['assuntoConviteSuspensoLojista'] = "Sua afiliação com o afiliado {nome_afiliado} foi suspensa";
$lang['msgAfiliacaoSuspensaLojista'] = "<p>Olá {nome_loja}</p><p>A sua afiliação com o afiliado {nome_afiliado} foi suspensa</p>";
$lang['msgAfiliacaoSuspensaLojistaBotao'] = "Acesse o app Slingfy para ver detalhes";
$lang['msgAfiliacaoSuspensaLojistaFooter'] = "Esta afiliação foi suspensa, mas não se preocupe, outras oportunidades virão :)";

$lang['lojas'] = "Lojas";

$lang['imagens'] = "Imagens";
$lang['imagem'] = "Imagem";
$lang['linkDivulgacao'] = "Link Afiliado";

$lang['produtos'] = "Produtos";

$lang['filtro'] = "Filtro";
$lang['filtroPlaceholder'] = "Busca pelo nome do produto";

$lang['geradorDeLink'] = "Gerador de link de afiliação";
$lang['geradorDeLinkPlaceholder'] = "Cole um link da loje para gerar o link de afiliacao";
$lang['gerar'] = "Gerar";
$lang['linkGeradoSucesso'] = "Link de afiliado gerado";
$lang['linkGeradoErro'] = "Url inválida";
$lang['jaELinkAfiliado'] = "Este já é um link de afiliado";
$lang['metricasGerais'] = "Métricas gerais";
$lang['todasAsVendas'] = "Todas as vendas dos últimos seis meses";
$lang['todasAsVisitas'] = "Todas as visitas dos últimos seis meses";
$lang['valorTotal'] = "Valor total";

$lang['unicas'] = "Ùnicas";
$lang['retornos'] = "Retornos";

$lang['todosConvites'] = "Todos convites";
$lang['periodo'] = "Período";
$lang['downloadEstatisticas'] = "Baixar Dados ( XLS )";
$lang['visitasOuOrdens'] = "Visitas ou Ordens";
$lang['visitas'] = "Visitas";
$lang['ordens'] = "Ordens";
$lang['selecione'] = "Selecione...";
$lang['selecioneVisitasOuOrdens'] = "Você deve escolher entre visistas ou ordens";
$lang['todasVendasRelatorio'] = "Todas as vendas";
$lang['idConvite'] = "ID convite";
$lang['orderId'] = "ID Ordem";
$lang['valorTotal'] = "Valor Total Ordem";
$lang['statusPagamento'] = "Status pagamento";
$lang['nomeLoja'] = "Nome Loja";
$lang['dataCriada'] = "Data de criação";
$lang['dataCriadaGMT'] = "Data de criação (GMT Ajustado)";
$lang['dataAtualizada'] = "Data última atualização";
$lang['dataAtualizadaGMT'] = "Data última atualização (GMT Ajustado)";
$lang['correcaoGMT'] = "Seu GMT:";


$lang['todasVisitasRelatorio'] = "Todas as Visitas";
$lang['tipoVisita'] = "Tipo visita";
$lang['unica'] = "Única";
$lang['retorno'] = "Retorno";
$lang['retorno'] = "Retorno";

$lang['notasAdicionais'] = "Venda realizada por: {nomeUsuario}\n Id convite: {id_convite}\n Nome Convite";

$lang['convite'] = "Convite";
$lang['logout'] = "Sair";
$lang['pagamentos'] = "Pagamentos";

$lang['configPagamentos'] = "Configurações de pagamento";
$lang['limiteDeGasto'] = "Limite de gasto ( USD )";
$lang['limiteGastosHelp'] = "Escolha o limite máximo de cobrança que podemos efetuar por mês ( em dólares americanos )";
$lang['cadastrarLimite'] = "Cadastrar Limite";
$lang['limiteAtutal'] = "Limite atual";
$lang['termos_billing'] = "Serão cobrados 0.5% das ordens pagas e rastreadas pelo Slingfy";
$lang['valor_minimo_pagamento'] = "Valor mínimo U$ 10.00";

$lang['exportarDados'] = "Exportar dados";
$lang['convitesHelp'] = "Caso queira associar a cobrança a algum convite específico, selecione acima";  

$lang['alerta'] = "Alerta";
$lang['cobrancaPendente'] = "O seu limite de está pendente de aprovação";
$lang['aprovar'] = "Aprovar";
$lang['valorCobranca'] = "Valor:";
$lang['status'] = "Status";
$lang['configureSeuPagamento'] = "Você deve configurar seu pagamento para começar a usar o Slingfy";
$lang['configureSeuPagamentoTrial'] = "Se for a primeira vez que estiver utilizando o Slingfy, você tem 15 dias grátis";
$lang['situacaoAtual'] = "Situação atual";
$lang['voceUtilizou'] = "Você utilizou ";
$lang['doSeuLimite'] = " do seu limite";

$lang['atualizarLimite'] = "Atualizar Limite";
$lang['algoPendente'] = "Existe algo pendente, impossível de realizar a ação agora";
$lang['alterarValorLimite'] = "Você pode alterar o valor de limite depois da aprovação, a qualquer momento";
$lang['naoExisteRecorrenciaAtiva'] = "Mudança de limite somente com recorrência ativa";
$lang['novoValorMaiorQueLimite'] = "O novo valor não pode ser que o limite já utilizado";


$lang['certezaQueDesejaCancelar'] = "Você tem certeza que deseja cancelar esse convite?";

$lang['ajuda'] = "Ajuda";
$lang['jaInstalado'] = "SlingFy já está instalado";

$lang['extrato'] = "Ordem Rastreada Paga - ID da Ordem: {orderId} - ID Convite: {conviteId}";


$lang['previewAtencaoAoPagamento'] = "Slingfy - Houve um problema com pagamento";
$lang['assuntoConviteSuspensoLojista'] = "Houve algum problema com o pagamento junto a Shopify";
$lang['msgAfiliacaoSuspensaLojista'] = "<p>Olá {nome_loja}</p><p>Não conseguimos efetuar a cobrança da order rastreada</p>";
$lang['msgAfiliacaoSuspensaLojistaBotao'] = "Acesse o app Slingfy para resolver";
$lang['msgAfiliacaoSuspensaLojistaFooter'] = "Não se preocupe, sua ordem será processada novamente assim que a situação for normalizada";
$lang['afiliadoNaoAceitouConvite'] = "Convite não aceito - O Slingfy não cobrou por essa ordem";

$lang['convitesPendentes'] = "Você tem convites pendentes, vá ao menu Convites para tomar uma ação";